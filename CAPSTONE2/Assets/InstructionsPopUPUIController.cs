﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsPopUPUIController : MonoBehaviour {

    public string EventName;
    public GameObject PopUp;

    bool Active;

    private void Start()
    {
        LevelEventHandler.StartListening(EventName, EnableObject);
        Active = false;
        PopUp.SetActive(false);
    }

    private void EnableObject()
    {
        Active = true;
        PopUp.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && Active)
        {
            GameInstance.GetActiveGameMode<LevelScene>().Player.GetComponent<UserControl>().enabled = true;
            PopUp.SetActive(false);
            LevelEventHandler.StopListening(EventName, EnableObject);
        }
    }

    private void OnDestroy()
    {
        LevelEventHandler.StopListening(EventName, EnableObject);
    }
}
