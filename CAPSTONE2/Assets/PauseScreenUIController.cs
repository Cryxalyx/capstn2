﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseScreenUIController : MonoBehaviour {

    private List<GadgetData> gadgetData;
    private GadgetData currentGadget;
    private int currentIndex;
    private PlayerUnit playerUnit;

    public GameObject FirstSelected;
    public Image Icon;
    public Text GadgetName;
    public Text DescriptionTextBox;
    public Text UniqueText;
    public Text DamageTextBox;
    public Text CostTextBox;

	// Use this for initialization
	void Awake () {
        SingletonManager.Register(this);
        gadgetData = new List<GadgetData>();
        playerUnit = GameInstance.GetActiveGameMode<LevelScene>().Player;
    }

    private void Start()
    {
        playerUnit.SetUpHealth();
        playerUnit.GetComponent<BlasterControl>().UiSetUp();
        if (gadgetData.Count == 0)
            ClearUI();
        this.gameObject.SetActive(false);
    }

    public void AddGadget(GadgetData gadget)
    {
        GadgetData temp = Instantiate(gadget);
        currentGadget = temp;
        gadgetData.Add(temp);
        currentIndex = gadgetData.Count - 1;
        UpdateUI();
    }

    public void MoveGadgetIndex(int index)
    {
        if (currentIndex + index < 0)
            currentIndex = gadgetData.Count - 1;
        else if (currentIndex + index >= gadgetData.Count)
        {
            currentIndex = 0;
        }
        else
            currentIndex = currentIndex + index;
        
        currentGadget = gadgetData[currentIndex];
        UpdateUI();

    }

    private void OnEnable()
    {
        if(gadgetData.Count != 0)
            UpdateUI();
        EventSystem.current.SetSelectedGameObject(FirstSelected);
    }

    void ClearUI()
    {
        Icon.sprite = null;
        GadgetName.text = null;
        DescriptionTextBox.text = null;
        UniqueText.text = null;
        DamageTextBox.text = null;
        CostTextBox.text = null;
    }

    public void UpdateUI()
    {
        Icon.sprite = currentGadget.Icon;
        GadgetName.text = currentGadget.Name;
        DescriptionTextBox.text = currentGadget.Description;
        UniqueText.text = currentGadget.UniqueSkill;
        DamageTextBox.text = currentGadget.Damage.ToString();
        CostTextBox.text = currentGadget.Cost.ToString();
    }

    private void OnDestroy()
    {
        foreach(GadgetData gd in gadgetData)
        {
            Destroy(gd);
        }
        gadgetData.Clear();
    }
}
