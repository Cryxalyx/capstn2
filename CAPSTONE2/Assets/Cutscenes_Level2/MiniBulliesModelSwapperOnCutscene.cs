﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBulliesModelSwapperOnCutscene : MonoBehaviour {

    [SerializeField] GameObject[] realMiniBullies;
    [SerializeField] GameObject[] cutsceneMiniBullies;

    private void OnEnable()
    {
        foreach (GameObject bully in realMiniBullies)
        {
            bully.SetActive(false);
        }

        foreach (GameObject bully in cutsceneMiniBullies)
        {
            bully.SetActive(true); 
        }
    }

    private void OnDisable()
    {
        foreach (GameObject bully in realMiniBullies)
        {
            bully.SetActive(true);
        }

        foreach (GameObject bully in cutsceneMiniBullies)
        {
            bully.SetActive(false);
        }
    }
}
