﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class AudioEvent : UnityEvent<int>
{

}

public class AudioEventManager : MonoBehaviour {

    private Dictionary<string, AudioEvent> eventDictionary;

    private static AudioEventManager uiUpdater;

    public static AudioEventManager instance
    {
        get
        {
            if (!uiUpdater)
            {
                uiUpdater = FindObjectOfType(typeof(AudioEventManager)) as AudioEventManager;

                if (!uiUpdater)
                {
                    Debug.LogError("There needs to be one active MemoryManager script on a GameObject in your scene.");
                }
                else
                {
                    uiUpdater.Init();
                }
            }

            return uiUpdater;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, AudioEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction<int> listener)
    {
        AudioEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new AudioEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<int> listener)
    {
        if (uiUpdater == null) return;
        AudioEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, int value)
    {
        AudioEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(value);
        }
    }
}
