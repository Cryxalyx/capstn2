﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Settings : MonoBehaviour {

    GameSettings gameSettings;
    float bgmValue;
    float sfxValue;

    public GameObject FirstSelected;

    public Toggle XInvert;
    public Toggle YInvert;
    public Slider BGMSlider;
    public Slider SFXSlider;

	// Use this for initialization
	void Start () {
        gameSettings = SingletonManager.Get<GameSettings>();
        gameSettings.BGM_Master.GetFloat("Volume", out bgmValue);
        gameSettings.BGM_Master.GetFloat("Volume", out sfxValue);
        BGMSlider.value = bgmValue;
        SFXSlider.value = sfxValue;
        XInvert.isOn = gameSettings.InvertX;
        YInvert.isOn = gameSettings.InvertY;
        gameObject.SetActive(false);
    }

    public void UpdateBGMVolume(float volume)
    {
        gameSettings.BGM_Master.SetFloat("Volume", volume);
    }

    public void UpdateSFXVolume(float volume)
    {
        gameSettings.SFX_Master.SetFloat("Volume", volume);
    }

    public void InvertXCam(bool x)
    {
        gameSettings.InvertX = x;
    }

    public void InvertYCam(bool y)
    {
        gameSettings.InvertY = y;
    }
}
