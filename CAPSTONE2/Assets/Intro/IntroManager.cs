﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour {

	[SerializeField] GameObject cutScene;
	[SerializeField] GameObject introText;
    AudioSource m_as;

    private void Start()
    {
        m_as = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
		if (introText != null && introText.transform.position.y >= 20.0f) {
			cutScene.SetActive (true);
			Destroy (introText.gameObject);
            m_as.Stop();
		}
	}

    
}
