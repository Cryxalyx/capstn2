﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoManager : MonoBehaviour {

    public NextLevelTrigger nxtLevel;
    IntroScene intro;
	public List<GameObject> videos;
    public Text skipText;
	float currTime = 0.0f;
	float time;
    AudioSource m_as;

	// Use this for initialization
	void Start () {
        m_as = GetComponent<AudioSource>();
        if (videos.Count != 0) {
			time = (float)videos [0].GetComponent<VideoPlayer>().clip.length;
			videos [0].SetActive (true);
            skipText.gameObject.SetActive(true);
            m_as.Play();
        }
        intro = GameScene.GetActive<IntroScene>();
	}

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Start") || Input.GetButtonDown("Jump"))
            SkipIntro();

		if (currTime >= time && videos.Count != 0) {

			GameObject video = videos [0];
			videos.Remove (video);
			Destroy (video);
			if (videos.Count != 0) {
				videos [0].SetActive (true);
				time = (float)videos [0].GetComponent<VideoPlayer> ().clip.length;
				currTime = 0.0f;
			} else {
                //nxtLevel.LoadNextScene();
                intro.OnLoadNext();
				skipText.gameObject.SetActive(false);
				Destroy(this);
			}
		} else {
			currTime += Time.deltaTime;
		}
	}

    public void SkipIntro()
    {
        skipText.gameObject.SetActive(false);
        for (int i = 0; i < videos.Count; i++)
        {
            Destroy(videos[i].gameObject);
        }
        videos.Clear();
        //nxtLevel.LoadNextScene();
        intro.OnLoadNext();
        skipText.gameObject.SetActive(false);
        Destroy(this);
    }
}
