﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarwarsIntro : MonoBehaviour {

	Transform m_transform;
	[SerializeField] float speed;
    float actualSpeed;
	// Use this for initialization
	void Start () {
		m_transform = GetComponent<Transform> ();
        actualSpeed = speed;

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Jump"))
            actualSpeed = speed * 4;
        else
            actualSpeed = speed;
		m_transform.position += m_transform.up * actualSpeed * Time.deltaTime;
	}
}
