﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIController : MonoBehaviour {

    public GameObject FirstUI;
    public List<GameObject> UIs; 
    
    public void OpenMenu()
    {
        FirstUI.SetActive(true);
    }

    public void CloseMenu()
    {
        foreach (GameObject ui in UIs)
            ui.SetActive(false);
    }

    private void OnDisable()
    {
        FirstUI.SetActive(false);
    }
}
