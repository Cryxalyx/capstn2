﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformModDisabler : MonoBehaviour {

    public PlatformModifier platmod;
    public string EventName;
	// Use this for initialization
	void Start () {
        if (platmod == null && GetComponent<PlatformModifier>() != null)
            platmod = GetComponent<PlatformModifier>();
        LevelEventHandler.StartListening(EventName, DisableMod);

    }

    void DisableMod()
    {
        platmod.enabled = false;
    }
}
