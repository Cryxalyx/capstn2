﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class PauseScreenHandler : MonoBehaviour {

    public List<GameObject> SkillDescriptions;
    public GameObject PauseSelected;
    public bool m_desc;
    float m_index;
    float m_maxIndex;
	// Use this for initialization
	void Start () {
	}


    void DisableCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void EnableCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnEnable()
    {
        EnableCursor();
    }

    private void OnDisable()
    {
        DisableCursor();
    }
}
