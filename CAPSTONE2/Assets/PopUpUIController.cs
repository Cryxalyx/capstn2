﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpUIController : MonoBehaviour {

    [Header("Event Trigger Code")]
    public string EventName;

    [Header("Other Events to Be Called")]
    public List<string> OtherEvents;
    public GameObject PopUp;

    bool Active;

    private void Start()
    {
        LevelEventHandler.StartListening(EventName, EnableObject);
        Active = false;
        PopUp.SetActive(false);
    }

    private void EnableObject()
    {
        Active = true;
        PopUp.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Jump") && Active)
        {
            GameInstance.GetActiveGameMode<LevelScene>().Player.GetComponent<UserControl>().enabled = true;
            PopUp.SetActive(false);
            LevelEventHandler.StopListening(EventName, EnableObject);
            foreach (string evt in OtherEvents)
                LevelEventHandler.TriggerEvent(evt);
        }
	}

    private void OnDestroy()
    {
        LevelEventHandler.StopListening(EventName, EnableObject);
    }
}
