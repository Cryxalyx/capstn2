﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorVisualize : MonoBehaviour {

    public Color color;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
