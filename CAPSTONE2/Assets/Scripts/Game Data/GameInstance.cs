﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;


public class GameInstance : MonoBehaviour
{
    private static GameInstance instance;

    public string FirstScene;

    private GameScene activeGameMode;

    static AsyncOperation async;

    public LoadingScreen LoadingScreenObject;

    private void Awake()
    {
        Assert.IsNull(instance, "There can only be one GameInstance");
        instance = this;
    }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine(StartTask());
    }

    IEnumerator StartTask()
    {
        yield return null;
        if (!string.IsNullOrEmpty(FirstScene)) yield return LoadGameMode(FirstScene);
    }

    public static GameScene GetGameMode()
    {
        return instance.activeGameMode;
    }

    public static T GetActiveGameMode<T>()
        where T : GameScene
    {
        return instance.activeGameMode as T;
    }

    /// <summary>
    /// Loads the scene for the specified GameMode
    /// This unloads active GameMode
    /// </summary>
    /// <param name="gameMode">Must be included in BuildSettings</param>
    /// <returns></returns>

    /// <summary>
    /// Loads the scene for the specified GameMode
    /// This unloads active GameMode
    /// </summary>
    /// <param name="gameMode">Must be included in BuildSettings</param>
    /// <param name="data">Optional data to be provided to the GameMode</param>
    /// <returns></returns>
    public static Coroutine LoadGameMode(string gameMode, IDictionary<string, object> data = null)
    {
        
        return instance.StartCoroutine(LoadGameModeTask(gameMode, data));
    }

    static IEnumerator LoadGameModeTask(string gameMode, IDictionary<string, object> data)
    {
        // Unload active game mode
        if (instance.activeGameMode)
        { 
            instance.LoadingScreenObject.Show();
            yield return new WaitForSeconds(1.0f);
            yield return instance.activeGameMode.Unload();
            yield return SceneManager.UnloadSceneAsync(instance.activeGameMode.gameObject.scene);
            instance.activeGameMode = null;
        }
        
        // Load next game mode
        yield return SceneManager.LoadSceneAsync(gameMode, LoadSceneMode.Additive);

        // Configure active game mode
        instance.activeGameMode = FindObjectsOfType<GameScene>().Single();
    }
}
