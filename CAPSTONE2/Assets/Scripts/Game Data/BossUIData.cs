﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName ="UIData/BossUIData")]
public class BossUIData : ScriptableObject {

    public Sprite Icon;
}
