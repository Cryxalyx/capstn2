﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScene : MonoBehaviour {

    /// <summary>
    /// List of all additive scenes that are bundled with the GameMode's main scene
    /// </summary>
    [SerializeField] private string[] additiveScenes = { };


    #region Overridables
    /// <summary>
    /// GameMode is still loading
    /// Additive scenes are already loaded at this point
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator OnLoad() { yield break; }
    /// <summary>
    /// GameMode is about to unload
    /// You can invoke saving of data as the game will wait for this coroutine to finish.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator OnUnload() { yield break; }
    #endregion

    #region Static Methods
    public static T GetActive<T>()
        where T : GameScene
    {
        return GameInstance.GetActiveGameMode<T>();
    }

    public static GameScene GetActive()
    {
        return GameInstance.GetGameMode();
    }

    public static bool IsGameMode<T>()
        where T : GameScene
    {
        return GameInstance.GetGameMode() is T;
    }
    #endregion

    private void Start()
    {
        StartCoroutine(LoadTask());
        LoadingScreen loading = SingletonManager.Get<LoadingScreen>();
        if (loading != null)
            loading.Hide();
    }

    IEnumerator LoadTask()
    {

        // Load additive scenes
        foreach (string sceneName in additiveScenes)
        {
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        }
        yield return null;

        // Set this scene as active
        SceneManager.SetActiveScene(gameObject.scene);
        yield return StartCoroutine(OnLoad());
    }

    public Coroutine Unload()
    {
        return StartCoroutine(UnloadTask());
    }

    IEnumerator UnloadTask()
    {
        yield return StartCoroutine(OnUnload());

        // Unload additive scenes
        foreach (string sceneName in additiveScenes)
        {
            yield return SceneManager.UnloadSceneAsync(sceneName);
        }
    }
}
