﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TitleScreen : GameScene
{
    public string NextScene;
    public GameObject FirstSelected;

    public void OnClick()
    {
        StartGame();
    }

    public void Quit()
    {
        Application.Quit();
    }

    Coroutine StartGame()
    {
        EventSystem.current.SetSelectedGameObject(FirstSelected);
        return StartCoroutine(StartGameTask());
    }

    IEnumerator StartGameTask()
    {
        yield return GameInstance.LoadGameMode(NextScene);
    }
}
