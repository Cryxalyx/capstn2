﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameSettings : MonoBehaviour {

    public bool InvertX;
    public bool InvertY;
    public AudioMixer BGM_Master;
    public AudioMixer SFX_Master;

    private void Awake()
    {
        SingletonManager.Register(this);
    }
}
