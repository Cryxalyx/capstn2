﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScene : GameScene {

    public string NextScene;

    public void OnLoadNext()
    {
        StartGame();
    }

    Coroutine StartGame()
    {
        return StartCoroutine(StartGameTask());
    }

    IEnumerator StartGameTask()
    {
        yield return GameInstance.LoadGameMode(NextScene);
    }
}
