﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScene : GameScene {

    public PlayerUnit Player;
    public AudioSource BGM_Source;

    protected override IEnumerator OnLoad()
    {
        SingletonManager.Get<AudioManager>().SetLevelAudioSource(BGM_Source);
        return base.OnLoad();
    }
}
