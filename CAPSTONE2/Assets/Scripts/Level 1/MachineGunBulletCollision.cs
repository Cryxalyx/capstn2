﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBulletCollision : MonoBehaviour {

	[SerializeField]float m_damage;

	public float Damage
	{
		get { return m_damage; }
		set { m_damage = value; }
	}

	// Use this for initialization
	void Start () {

	}

	private void OnParticleCollision(GameObject other)
	{
		if (other.GetComponent(typeof(IDamageable)) != null)
		{

			IDamageable damageable = (IDamageable)other.GetComponent(typeof(IDamageable));
			damageable.TakeDamage(m_damage);
		}

	}
}
