﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

	public GameObject projectile;
	public float bulletSpeed;
	public Transform[] spawnPoint;
	public float damage;
	public GameObject target;
	public float bulletSpawnTime;
	protected Vector3 targetPos;

	// Use this for initialization
	public virtual void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

	public virtual void Fire() {
		
	}



	public virtual void SetTarget(GameObject other) {
		RaycastHit hit;
		if (!other) {
			target = null;
		}
		target = other;
	}
}
