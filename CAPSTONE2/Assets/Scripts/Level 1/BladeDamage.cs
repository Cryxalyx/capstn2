﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeDamage : MonoBehaviour {

	public float bladeDamage = 5.0f;

	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.GetComponent(typeof(IDamageable)) != null)
		{
			IDamageable dmg = collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
			dmg.TakeDamage(bladeDamage);
		}
	}
}
