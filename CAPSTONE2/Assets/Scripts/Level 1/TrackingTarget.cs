﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingTarget : MonoBehaviour {

	public GameObject target = null;
	public CannonCatapult cannon;

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.GetComponent<PlayerUnit>() != null) {
			cannon.SetTarget (other.gameObject);
			target = other.gameObject;
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.GetComponent<PlayerUnit>() != null) {
			cannon.SetTarget(null);
			target = null;
		}
	}
}
