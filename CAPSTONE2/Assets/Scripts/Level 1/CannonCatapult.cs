﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonCatapult : Turret {

	public float altitude;
	//public float maxAltitude;

	// Use this for initialization
	public override void Start () {
		StartCoroutine (CannonInterval (bulletSpawnTime));
	}

	public override void Update() {
		if (target != null) {
			Vector3 look = new Vector3 (target.transform.position.x, target.transform.position.y + Vector3.Distance (transform.position, target.transform.position) * 0.5f, target.transform.position.z);
			transform.LookAt (look);
		}
	}

	public override void Fire() {
		if (target == null)
			return;

		targetPos = target.transform.position;

		for (int i = 0; i < spawnPoint.Length; i++) {
			GameObject bullet = Instantiate (projectile);
			bullet.transform.position = spawnPoint[i].position;
			
			bullet.GetComponent<Projectiles> ().targetPosition = targetPos;
			bullet.GetComponent<CannonProjectile> ().m_Damage = damage;
			Rigidbody rb = bullet.GetComponent<Rigidbody> ();
			Vector3 velocity = CalculateLaunchVelocity(bullet);
			rb.velocity = velocity;
		}
	}

	Vector3 CalculateLaunchVelocity(GameObject bullet) {
		//altitude = targetPos.y + (maxAltitude - Vector3.Distance(bullet.transform.position, targetPos) * 0.5f);
		float displacementY = targetPos.y - bullet.transform.position.y;
		Vector3 displacementXZ = new Vector3(targetPos.x - bullet.transform.position.x, 0, targetPos.z - bullet.transform.position.z);

		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * Physics.gravity.y * altitude);
		Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt (-2 * altitude / Physics.gravity.y) + Mathf.Sqrt (2 * (displacementY - altitude) / Physics.gravity.y));
			
		return (velocityY + velocityXZ);
	}

	IEnumerator CannonInterval(float interval) {
		while (true) {
			yield return new WaitForSeconds (interval);
			Fire ();
		}
	}

}
