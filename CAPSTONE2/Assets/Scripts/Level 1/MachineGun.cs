﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : Turret {

	public List<ParticleSystem> m_ParSys;
	public List<MachineGunBulletCollision> m_Bullets;
	bool isDone = false;
	//float currTime = 0.0f;
	float m_ParSysTimer = 0.0f;
	public bool toggleCanTarget;
	public float bulletDuration;
	bool isCalled = false;

	// Use this for initialization
	public override void Start () {
	}

	// Update is called once per frame
	public override void Update () {
		if(!isCalled)
			StartCoroutine (BulletSpawn ());
		
	}

	public override void Fire() {
		if (toggleCanTarget)
			targetPos = target.transform.position;
		else
			targetPos = transform.forward;
		
		foreach (var bullet in m_Bullets) {
			bullet.Damage = this.damage;
		}
		foreach (ParticleSystem par in m_ParSys) {
			par.Play ();
		}
		isDone = true;
		
	}

	IEnumerator BulletSpawn() {
		isCalled = true;
		if (!isDone) {
			Fire ();
			yield return null;
		} else {
			m_ParSysTimer += Time.deltaTime;
			if (m_ParSysTimer > bulletDuration) {
				Reset ();
				yield return new WaitForSeconds (bulletSpawnTime);
			}
			yield return null;
		}
	}

	void Reset() {
		foreach (ParticleSystem par in m_ParSys) {
			par.Stop ();
		}
		m_ParSysTimer = 0.0f;
		isDone = false;
	}
}
