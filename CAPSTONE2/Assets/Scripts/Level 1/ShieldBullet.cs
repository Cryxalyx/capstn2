﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBullet : BlasterBullet, IDamageable {

	public float m_shieldHealth;
	bool m_shot;

	bool delayed;
	float timer;
	bool bound;
	public float delay;

	// Use this for initialization
	void Start()
	{
		path = "Prefabs/ShieldBall";
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (!delayed)
			return;
		
		if (collision.gameObject.GetComponent<PlayerUnit> () != null) {
			//gameObject.GetComponent<Collider> ().isTrigger = true;
			transform.SetParent (collision.gameObject.transform);
            gameObject.layer = 17;
			bound = true;
		}
	}

	public virtual void TakeDamage(float damage) {
		Shield (damage);
	}

	public void Shield(float damage)
	{
		m_shieldHealth -= damage;
		if (ShieldBreak ()) {
			Destroy (gameObject);
			m_lifeTimer = 0;
			m_shot = false;
		}

	}

	bool ShieldBreak()
	{
		return m_shieldHealth <= 0;
	}

	public override void Fire(Vector3 force)
	{
		if (!m_shot) {
			base.Fire (force);
			m_shot = true;
		}
		
	}

	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime;
		if (timer > delay)
			delayed = true;

		if (bound)
			transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + 0.4f, transform.parent.position.z);


		if (Disperse())
		{
			Destroy(gameObject);
			m_lifeTimer = 0;
			m_shot = false;
		}

	}
}
