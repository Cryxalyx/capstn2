﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevolvingBlade : MonoBehaviour {

	public Transform[] wayPoint;
	public Vector3 origin;
	public Vector3 endPoint;
	public float speed;
	float curValue = 0.0f;
	float maxvalue = 1.0f;
	bool reverse = false;
	public int startingIndex;

	// Use this for initialization
	void Start () {
		origin = wayPoint [startingIndex].position;
		if (startingIndex + 1 >= wayPoint.Length) {
			endPoint = wayPoint [startingIndex - 1].position;
			reverse = true;
		} else
			endPoint = wayPoint [startingIndex + 1].position;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void Move() {

		curValue += Time.deltaTime;
		transform.position = Vector3.Lerp (origin, endPoint, (curValue/maxvalue) * speed);
		if (transform.position == endPoint) {
			ChangeDirection ();
			curValue = 0.0f;
		}
	}

	void ChangeDirection() {
		if (!reverse) {
			for (int i = 0; i < wayPoint.Length; i++) {
				if (endPoint == wayPoint [i].position) {
					origin = wayPoint[i].position;

					if (i + 1 >= wayPoint.Length) {
						endPoint = wayPoint [i - 1].position;
						reverse = true;
					} else
						endPoint = wayPoint [i + 1].position;
					
					break;
				}
			}
		} else {
			for (int i = wayPoint.Length - 1; i >= 0; i--) {
				if (endPoint == wayPoint [i].position) {
					origin = wayPoint[i].position;
					if (i - 1 < 0) {
						endPoint = wayPoint [i + 1].position;
						reverse = false;
					} else
						endPoint = wayPoint [i - 1].position;
					break;
				}
			}
		}
	}
}
