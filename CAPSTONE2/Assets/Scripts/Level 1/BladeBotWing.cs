﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeBotWing : MonoBehaviour {

	public List<GameObject> m_blades;
	public float interval;
	public bool forward;
	public Transform origin, endPos;

	// Use this for initialization
	void Start () {
		StartCoroutine (DelayStart(interval));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator DelayStart(float interval) {
		for (int i = 0; i < m_blades.Count; i++) {
			m_blades [i].AddComponent (typeof(BladeSimpleAI));
			if (forward) {
				BladeSimpleAI ai = m_blades [i].GetComponent<BladeSimpleAI> ();
				ai.origin = origin;
				ai.endPos = endPos;
			}
			
			yield return new WaitForSeconds (interval);
		}
	}
}
