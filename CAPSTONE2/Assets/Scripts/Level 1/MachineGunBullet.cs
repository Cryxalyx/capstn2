﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBullet : Projectiles {

	public float m_Damage;
	public float m_LifeSpan;
	float m_timer;

	private void OnCollisionEnter(Collision collision)
	{
		MonoBehaviour[] mbList = collision.gameObject.GetComponentsInChildren<MonoBehaviour>();

		foreach (MonoBehaviour mb in mbList)
		{
			if (mb is IDamageable)
			{        
				IDamageable damageable = (IDamageable)mb;
				damageable.TakeDamage(m_Damage);
			}
		}
		Destroy(gameObject);

	}

	void Update() {
		m_timer += Time.deltaTime;
		if(m_timer >= m_LifeSpan)
		{
			Destroy(this.gameObject);
		}
	}
}
