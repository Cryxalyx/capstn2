﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour {

	public Transform pointA, pointB;

	// Update is called once per frame
	void Update () {
		this.transform.position = Vector3.Lerp (pointA.position, pointB.position, (Mathf.Abs (Mathf.Sin (Time.time) + 1.0f) * 0.5f));
	}
}
