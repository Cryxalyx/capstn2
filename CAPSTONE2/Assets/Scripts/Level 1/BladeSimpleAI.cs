﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeSimpleAI : MonoBehaviour {

	public Transform origin;
	public Transform endPos;
	public float speed;
	public bool forward;

	protected Vector3 m_start;
	protected Vector3 m_end;
	protected float curValue;
	protected float maxValue = 1.0f;

	// Use this for initialization
	public virtual void Start () {
		m_start = origin.position;
		m_end = endPos.position;
		curValue = 0.0f;
	}

	// Update is called once per frame
	public virtual void Update () {
		Move (m_start, m_end);
	}

	protected void Move(Vector3 start, Vector3 end) {
		curValue += Time.deltaTime;
		transform.position = Vector3.MoveTowards (start, end, (curValue/maxValue) * speed);
		if (transform.position == m_end) {
			forward = !forward;
			ChangeDirection ();
			curValue = 0.0f;
		}
	}

	protected void ChangeDirection() {
		Vector3 tmp = m_end;
		m_end = m_start;
		m_start = tmp;
	}
}
