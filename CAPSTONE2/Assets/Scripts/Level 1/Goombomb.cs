using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goombomb : DamagingUnit {

	[SerializeField] float damage;

	void OnCollisionEnter(Collision col) {
		GameObject tmp = col.collider.gameObject;

		if ((tmp.transform.position.y) >= (transform.position.y) ){
			Destroy (this.gameObject);
		} else if(tmp.GetComponent<PlayerUnit>() != null) {
			tmp.SendMessage ("TakeDamage", damage);
		}
	}
}
