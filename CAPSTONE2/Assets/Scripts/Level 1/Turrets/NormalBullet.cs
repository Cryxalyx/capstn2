﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBullet : AttackPattern {

	public List<ParticleSystem> m_ParSys;
	public List<MachineGunBulletCollision> m_Bullets;

	Transform m_transform;
	bool m_attack;
	float m_timer;
	public float bulletDuration;

	// Use this for initialization
	void Start () {
		m_transform = GetComponent<Transform>();
		//m_Flames.AddRange(GetComponentsInChildren<FlameCollision>());
		//foreach(FlameCollision fc in m_Flames)
		//{
		//    m_ParSys.Add(fc.GetComponent<ParticleSystem>());
		//}
	}

	public override IEnumerator Attack()
	{
		foreach (var bullet in m_Bullets)
		{
			bullet.Damage = GetComponent<DamagingUnit>().Damage;
		}

		foreach (ParticleSystem ps in m_ParSys)
		{

			ps.Play();
		}
		yield return StartCoroutine(Attacking());
		m_isFinished = true;
	}

	/// <summary>
	/// Supposed to be for rotation but is not smoothed out.
	/// </summary>
	/// <returns></returns>
	IEnumerator Attacking()
	{
		while(true)
		{
			m_timer = m_timer + Time.deltaTime;
			if (m_timer > bulletDuration)
			{
				m_timer = 0;
				break;
			}
			yield return null;
		}
		yield return null;
	}

	public override void StopAttack()
	{
		base.StopAttack();
		foreach(ParticleSystem ps in m_ParSys)
		{
			ParticleSystem.EmissionModule em = ps.emission;
			em.enabled = false;
		}
	}

	// Update is called once per frame
	void Update () {

	}
}
