﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehavior : DamagingUnit {

	protected AttackPattern m_currentAttack;

	int m_attackPatternIndex;
	float m_attackTransitionTime;
	public float m_BaseAttackTransitionTime;
	float m_attackIndexCounter;

	public List<AttackPattern> AttackPatterns;

	public AttackPattern CurrentAttackPattern
	{
		get { return m_currentAttack; }
		set { m_currentAttack = value; }
	}

	protected override void Start()
	{
		base.Start();
	}
	
	void UnitSetup()
	{
		
		Damage = BaseDamage;
		int size = GetComponents<AttackPattern>().Length;
		for (int i = 0; i < size; i++)
		{
			AttackPatterns.Add(GetComponents<AttackPattern>()[i]);
		}
		foreach (AttackPattern ap in AttackPatterns)
		{
			ap.target = target;
		}
	}

	public void Attack()
	{
		m_currentAttack = AttackPatterns[m_attackPatternIndex];
		StartCoroutine(m_currentAttack.Attack());
	}
}
