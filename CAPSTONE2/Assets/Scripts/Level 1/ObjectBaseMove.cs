﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBaseMove : MonoBehaviour {

	public Transform endPos;
	public float speed;
	public GameObject baseObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (baseObject == null) {
			transform.position = Vector3.MoveTowards (transform.position, endPos.position, Time.deltaTime * speed);
		}
	}
}
