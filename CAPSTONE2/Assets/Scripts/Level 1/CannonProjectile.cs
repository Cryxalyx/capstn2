﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonProjectile : Projectiles {

	public float m_Damage;

	private void OnCollisionEnter(Collision collision)
	{
		MonoBehaviour[] mbList = collision.gameObject.GetComponentsInChildren<MonoBehaviour>();

		foreach (MonoBehaviour mb in mbList)
		{
			if (mb is IDamageable)
			{        
				IDamageable damageable = (IDamageable)mb;
				damageable.TakeDamage(m_Damage);
			}
		}
		Destroy(gameObject);
	}
}
