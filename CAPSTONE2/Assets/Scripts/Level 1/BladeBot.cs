﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeBot : BladeSimpleAI {
	
	public override void Start ()
	{
		if (!forward) {
			m_start = new Vector3 (endPos.position.x, transform.position.y, transform.position.z);
			m_end = new Vector3 (origin.position.x, transform.position.y, transform.position.z);
		} else {
			m_start = new Vector3 (origin.position.x, transform.position.y, transform.position.z);
			m_end = new Vector3 (endPos.position.x, transform.position.y, transform.position.z);
		}
		curValue = InitialPosition ();
	}

	public override void Update ()
	{
		Move (m_start, m_end);
	}

	float InitialPosition() {
		float curDist = Vector3.Distance (transform.position, m_end);
		float maxDist = Vector3.Distance (m_start, m_end);

		return curDist / maxDist;
	}
}
