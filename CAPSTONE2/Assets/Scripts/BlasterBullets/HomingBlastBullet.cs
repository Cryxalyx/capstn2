﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Homing))]
public class HomingBlastBullet : MiniBullEBlastBullet {

	// Use this for initialization
	void Start () {
        
	}

    protected override void Update()
    {
        base.Update();
        if(GetComponent<Homing>().target == null)
            GetComponent<Homing>().SetTarget(MiniBParent.target);
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
    }
}
