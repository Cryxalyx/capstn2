﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBullEBlastBullet : EnergyBlastBullet {

    public DamagingUnit MiniBParent;

    protected override void OnCollisionEnter(Collision collision)
    {
        //remove if if you want to make it hit anyone.
        if (MiniBParent.target == collision.gameObject)
            DamageDamageable(collision);
        if (DestroyOnCollision)
            Destroy(gameObject);

    }

}
