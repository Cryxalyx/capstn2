﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterBlast : EnergyBlastBullet {

    public List<Transform> scatterPoints;
    public GameObject scatterSpawn;
	// Use this for initialization
	void Start () {
        

    }

    void Scatter()
    {
        if (scatterPoints.Count == 0 || scatterSpawn == null)
            return;
        foreach (Transform trans in scatterPoints)
        {
            GameObject projectile = Instantiate(scatterSpawn, trans.position, Quaternion.identity);
            projectile.GetComponent<EnergyBlastBullet>().Fire(trans.forward.normalized * 1000);
        }
    }

    protected override void Update()
    {
        if (Disperse())
        {
            Scatter();
            Destroy(gameObject);
            m_lifeTimer = 0;
        }
    }
}
