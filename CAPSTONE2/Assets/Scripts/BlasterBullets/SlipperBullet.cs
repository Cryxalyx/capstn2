﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlipperBullet : BlasterBullet, IAbsorbable
{
    public float m_damage;
    bool m_shot;
    // Use this for initialization
    void Start()
    {
        path = "Prefabs/Slipper";
    }

    private void OnCollisionEnter(Collision collision)
    {
        BreakBreakable(collision);
        //DamageDamageable(collision);
    }

    void BreakBreakable(Collision collision)
    {
        if (collision.gameObject.GetComponent(typeof(IBreakable)) as IBreakable == null)
            return;
        IBreakable breakable = collision.gameObject.GetComponent(typeof(IBreakable)) as IBreakable;
        breakable.Break();
        Destroy(gameObject);
    }

    void DamageDamageable(Collision collision)
    {
        if (collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable == null)
            return;
        if (collision.gameObject.GetComponent<PlayerUnit>() != null)
            return;
        IDamageable damageable = collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
        damageable.TakeDamage(m_damage);
        Destroy(gameObject);
    }

    public override void Fire(Vector3 force)
    {
        base.Fire(force);
        m_shot = true;

    }

    public void SetDamage(float damage)
    {
        m_damage = damage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Disperse())
        {
            Destroy(gameObject);
            m_lifeTimer = 0;
        }
        transform.Rotate(Vector3.up * -1 * 1000 * Time.deltaTime);
    }
}
