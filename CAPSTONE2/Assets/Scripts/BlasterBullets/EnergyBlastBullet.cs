﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBlastBullet : BlasterBullet {

    public GameObject m_OnHitUnitFeedback;
    public GameObject m_OnHitWallFeedback;
    public GameObject m_OnNoHitFeedback;
    public float m_damage;
    public bool DestroyOnCollision;  
    bool m_shot;
    bool m_hitUnit;
    // Use this for initialization
    void Start()
    {

    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        DamageDamageable(collision);
        if(DestroyOnCollision && !m_hitUnit)
        {
            if (m_OnHitWallFeedback != null) 
                Instantiate(m_OnHitWallFeedback, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
            
    }

    /// <summary>
    /// Void to Damage if object collided with is Damageable otherwise nothing happens.
    /// </summary>
    /// <param name="collision"></param>
    protected void DamageDamageable(Collision collision)
    {
        if (collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable == null)
            return;
        m_hitUnit = true;
        IDamageable damageable = collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
        damageable.TakeDamage(m_damage);
        if (m_OnHitUnitFeedback != null)
            Instantiate(m_OnHitUnitFeedback, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    /// <summary>
    /// Launched Projectile based on given force.
    /// </summary>
    /// <param name="force"></param>
    public override void Fire(Vector3 force)
    {
        base.Fire(force);
        m_shot = true;

    }

    /// <summary>
    /// Used to set damage outside of script mainly in Blaster Controller.
    /// </summary>
    /// <param name="damage"></param>
    public void SetDamage(float damage)
    {
        m_damage = damage;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Disperse())
        {
            if (m_OnNoHitFeedback != null)
                Instantiate(m_OnNoHitFeedback, transform.position, Quaternion.identity);
            Destroy(gameObject);
            m_lifeTimer = 0;
        }

    }
}
