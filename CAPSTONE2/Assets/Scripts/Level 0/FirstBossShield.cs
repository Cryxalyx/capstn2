﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBossShield : MonoBehaviour {

    [SerializeField] float m_charge;
	// Use this for initialization
	void Start () {
        LevelEventHandler.StartListening("PowerCellOut", ReduceCharge);
	}

    void ReduceCharge()
    {
        m_charge--;
    }

    private void OnDestroy()
    {
        LevelEventHandler.StopListening("PowerCellOut", ReduceCharge);
    }

    // Update is called once per frame
    void Update () {
        if (m_charge == 0)
            Destroy(gameObject);
	}
}
