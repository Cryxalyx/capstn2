﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearDamageTrigger : MonoBehaviour {

    public bool m_ToDoDamage;
	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnvironmentDmgTirgger>() != null)
        {
            other.GetComponent<EnvironmentDmgTirgger>().SetDamaging(m_ToDoDamage);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
