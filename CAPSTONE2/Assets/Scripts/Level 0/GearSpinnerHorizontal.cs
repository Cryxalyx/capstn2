﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Clockwise = 1,
    CounterClockwise = -1
}

public class GearSpinnerHorizontal : PlatformModifier
{

    //Transform m_Transform;

    [Range(0, 4)]
    public float m_Speed;
    public Direction m_Direction;

	// Use this for initialization
	void Awake () {
        //Transform[] m_transforms = GetComponentsInChildren<Transform>();
        //foreach (Transform t in m_transforms)
        //{
        //    if (t.parent != null)
        //    {
        //        m_Transform = t;
        //    }
        //}

    }

    public override void ModifyPlatform()
    {
        transform.Rotate(Vector3.up * (int)m_Direction * m_Speed);
    }

    // Update is called once per frame
    void FixedUpdate () {
        ModifyPlatform();
	}
}
