﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEventTrigger : MonoBehaviour {

    public List<Powercell> Powercells;
    int m_divider;
    BossBehavior bb;

	// Use this for initialization
	void Start () {
        bb = GetComponent<BossBehavior>();
        LevelEventHandler.StartListening("PowerCellOut", DamageBoss);
        m_divider = Powercells.Count * 2;

    }
	
    void DamageBoss()
    {
        bb.TakeDamage(bb.Health/ m_divider);
    }

	// Update is called once per frame
	void Update () {
        

        if (Powercells.Count == 0)
            Destroy(this);
	}

    private void OnDestroy()
    {
        LevelEventHandler.StopListening("PowerCellOut", DamageBoss);
    }
}
