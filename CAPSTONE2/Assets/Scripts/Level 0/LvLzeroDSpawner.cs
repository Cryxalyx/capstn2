﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvLzeroDSpawner : DebrisSpawner {

    public int MinSpawn;
    public int MaxSpawn;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ControlledMultiSpawn(int limit)
    {
        for (int i = 0; i < limit; i++)
        {

        }
    }

    public void RandomMultiSpawn()
    {
        int rand = Random.Range(MinSpawn, MaxSpawn);
        for (int i = 0; i < rand; i++)
        {

        }
    }
}


