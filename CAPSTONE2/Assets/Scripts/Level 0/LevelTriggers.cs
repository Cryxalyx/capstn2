﻿using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine;

public class LevelTriggers : MonoBehaviour {

    [SerializeField] UnityEvent m_event;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        m_event.Invoke();
    }

}
public class TriggerEvent : UnityEvent<int>
{

}