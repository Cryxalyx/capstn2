﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullEBlasterController : MonoBehaviour {

    public float BlasterCoolDown;
    public float BlasterShotSpeed;
    public List<MiniBullEBlaster> Blasters;
    public GameObject ProjectilePrefab;
    public bool Attack;
    bool m_coolDown;
    [SerializeField]float m_coolDownTimer;
    [SerializeField]int m_attackArm;
    public AudioSource m_as;

    public int AttackArm
    {
        get { return m_attackArm; }
        set { m_attackArm = value; }
    }

    public bool IsCoolDown
    {
        get { return m_coolDown; }
    }

    // Use this for initialization
    void Start () {
        Blasters.AddRange(GetComponentsInChildren<MiniBullEBlaster>());
        foreach(MiniBullEBlaster mbb in Blasters)
        {
            mbb.m_BullE = this.GetComponent<DamagingUnit>();
        }
	}

    public void Shoot()
    {
        if (m_coolDown)
            return;
        GetComponent<Animator>().SetFloat("AttackArm", m_attackArm);
        GetComponent<Animator>().SetBool("Attack", Attack);
    }

    public void SingleFire(int index)
    {
        m_as.Play();
        Blasters[index].Fire(ProjectilePrefab, BlasterShotSpeed, this.GetComponent<DamagingUnit>());
        Attack = false;
        m_coolDown = true;
    }

    //void EndAttack()
    //{
    //    GetComponent<Animator>().SetBool("Attack", Attack);
    //}

    bool CooledDown()
    {
        m_coolDownTimer += Time.deltaTime;
        GetComponent<Animator>().SetBool("Attack", false);
        return (m_coolDownTimer > BlasterCoolDown);
    }

    private void Update()
    {
        if (!m_coolDown)
        {
            m_coolDownTimer = 0;
            return;
        }
        if (m_coolDown)
            m_coolDown = !CooledDown();
    }
}
