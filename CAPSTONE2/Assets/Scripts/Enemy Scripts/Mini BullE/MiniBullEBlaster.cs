﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBullEBlaster : MonoBehaviour {

    public DamagingUnit m_BullE;
    public void Fire(GameObject Projectile, float speedMultiplier, DamagingUnit parent)
    {
        GameObject projectile = Instantiate(Projectile, this.transform.position, Quaternion.identity);
        if (projectile.GetComponent<Homing>() != null)
            projectile.GetComponent<Homing>().SetTarget(m_BullE.target);
        Vector3 direction = (parent.transform.forward.normalized * speedMultiplier) * 1000;
        
        projectile.GetComponent<Rigidbody>().AddForce(direction);
    }
}
