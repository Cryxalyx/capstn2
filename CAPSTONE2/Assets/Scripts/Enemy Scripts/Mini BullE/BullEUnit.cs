﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullEUnit : DamagingUnit {

    
    protected override void Start()
    {
        base.Start();
    }


    public override void TakeDamage(float damage)
    {
        if (m_invulnerable)
            return;
        base.TakeDamage(damage);
        m_invulnerable = true;
        GetComponent<CharacterControl>().TakenDamage(damage);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<PlayerUnit>() != null)
        {
            collision.gameObject.GetComponent<PlayerUnit>().TakeDamage(Damage);
            Vector3 force = collision.collider.transform.position - transform.position;
            force.y = 2;
            collision.collider.gameObject.GetComponent<Rigidbody>().AddForce(force.normalized * 5, ForceMode.Impulse);
        }
    }

    private void FixedUpdate()
    {

        if (m_invulnerable && GetComponent<CharacterControl>().HasRecovered())
            m_invulnerable = !Recovered();
        if (m_dead)
            this.gameObject.SetActive(!Disappear());
    }
}
