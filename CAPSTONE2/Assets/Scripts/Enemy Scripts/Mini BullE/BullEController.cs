﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BullEController : CharacterControl {

    void OnAnimatorMove()
    {
        //Remove m_grounded in condition to allow movement while in air.
        Vector3 v = (m_move * m_MovementMultiplier) / Time.deltaTime;
        if (m_grounded && Time.deltaTime > 0 && !m_damaged && !m_dead)
        {

            //to Ensure that current velocity y isn't overwritten by new value.
            v.y = m_rigidBody.velocity.y;
            m_rigidBody.velocity = v * (Time.fixedDeltaTime * 50);
        }
    }

    protected override void UpdateAnimator(Vector3 move)
    {
        m_animator.SetFloat("Forward", m_forwardAmount, 0.1f, Time.deltaTime);
        m_animator.SetInteger("Move", (int)m_rigidBody.velocity.magnitude);
        m_animator.SetBool("Grounded", m_grounded);
        m_animator.SetBool("Damaged", m_damaged);
    }

    public override void Die()
    {
        if (m_dead)
            return;
        base.Die();
        m_animator.SetBool("Dead", true);
        this.enabled = false;
    }

    private void FixedUpdate()
    {
        UpdateAnimator(m_move);

    }

    private void OnDrawGizmosSelected()
    {
        Debug.DrawLine((transform.position + Vector3.up * 0.1f), (transform.position + Vector3.down * hitdistance));
        Gizmos.DrawWireSphere((transform.position + Vector3.up * 0.1f) + (Vector3.down * hitdistance), 0.1f);
    }
}
