﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBossController : CharacterControl {

    Direction m_direction;

    #region Movement and Rotation

    public override void Move(Vector3 move, bool crouch, bool jump)
    {
        base.Move(move, crouch, jump);
    }

    public override void RotateCharacter(Vector3 move)
    {
        base.RotateCharacter(move);
    }
    #endregion

    public override void TakenDamage(float damagePercentage)
    {
        base.TakenDamage(damagePercentage);

    }
}
