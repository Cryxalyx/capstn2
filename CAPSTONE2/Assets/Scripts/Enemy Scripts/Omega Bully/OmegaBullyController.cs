﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmegaBullyController : CharacterControl {

    float m_accumulatedDamage;
    public float m_DamageThreshold;
    void OnAnimatorMove()
    {
        
        //Remove m_grounded in condition to allow movement while in air.
        Vector3 v = (m_move * m_MovementMultiplier) / Time.deltaTime;
        if (Time.deltaTime > 0 && !m_damaged && !m_dead)
        {
            //to Ensure that current velocity y isn't overwritten by new value.
            v.y = m_rigidBody.velocity.y;
            m_rigidBody.velocity = v * (Time.fixedDeltaTime * 50);
        }
    }


    bool TriggerDamage()
    {
        return (m_accumulatedDamage > m_DamageThreshold);
    }

    public override void TakenDamage(float damageAmount)
    {
        m_accumulatedDamage += damageAmount;
        if(TriggerDamage())
        {
            m_damaged = true;
            m_accumulatedDamage = 0;
        }
            
    }

    public override void Die()
    {
        base.Die();
        m_animator.SetBool("Dead", true);
    }

    protected override void UpdateAnimator(Vector3 move)
    {
        m_animator.SetFloat("Forward", m_forwardAmount);
        m_animator.SetInteger("Move", (int)m_rigidBody.velocity.magnitude);
        m_animator.SetBool("Grounded", m_grounded);
        m_animator.SetBool("Damaged", m_damaged);
    }

    // Update is called once per frame
    void Update () {
        UpdateAnimator(m_move);
        CheckGrounded();
    }
}
