﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevolvingProjectiles : MonoBehaviour {


    public GameObject parent;
    float speed;
	// Update is called once per frame
	void Update ()
    {

        if (parent != null)
        {
            transform.RotateAround(parent.transform.position, new Vector3(0, 3.0f, 0), speed);
        }
        speed += 0.02f;
	}
}
