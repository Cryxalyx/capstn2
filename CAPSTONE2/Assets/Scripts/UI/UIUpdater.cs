﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UISliderEvent : UnityEvent<float>
{

}

public class UIUpdater : MonoBehaviour {
    private Dictionary<string, UISliderEvent> eventDictionary;

    private static UIUpdater uiUpdater;

    public static UIUpdater instance
    {
        get
        {
            if (!uiUpdater)
            {
                uiUpdater = FindObjectOfType(typeof(UIUpdater)) as UIUpdater;

                if (!uiUpdater)
                {
                    Debug.LogError("There needs to be one active UIUpdater script on a GameObject in your scene.");
                }
                else
                {
                    uiUpdater.Init();
                }
            }

            return uiUpdater;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, UISliderEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction<float> listener)
    {
        UISliderEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UISliderEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<float> listener)
    {
        if (uiUpdater == null) return;
        UISliderEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, float value)
    {
        UISliderEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(value);
        }
    }
}
