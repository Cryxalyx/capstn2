﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUISlider : UISlider
{
    void Awake()
    {
        UIUpdater.StartListening("HealthSetUp", ValueSetUp);
        UIUpdater.StartListening("MaxHealthSetup", MaxValueSetUp);
    }
    // Use this for initialization
    void Start()
    {
        
    }
    /// <summary>
    /// Called on Awake via events.
    /// </summary>
    /// <param name="value">base value</param>
    void ValueSetUp(float value)
    {
        m_value = value;
        UIUpdater.StopListening("HealthSetUp", ValueSetUp);
    }

    void MaxValueSetUp(float value)
    {
        m_maxValue = value;
        valueUpdate();
        UIUpdater.StopListening("MaxHealthSetup", MaxValueSetUp);
    }

    private void OnEnable()
    {
        UIUpdater.StartListening("TakeDamage", ReduceValue);
        UIUpdater.StartListening("RestoreHealth", AddValue);
    }

    private void OnDisable()
    {
        UIUpdater.StopListening("TakeDamage", ReduceValue);
        UIUpdater.StopListening("RestoreHealth", AddValue);
    }

    protected override void valueUpdate()
    {
        base.valueUpdate();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
