﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISlider : MonoBehaviour {


	public Slider sliderBar;
	//public Color fullvalue, lowvalue;
	protected float m_value;
    protected float m_maxValue;



    /// <summary>
    /// To be used if and when saving is implemented.
    /// </summary>
    /// <param name="value"></param>
#region
    protected void UpdateCurrentValue(float value)
    {
        m_value = value;
    }

    protected void UpdateCurrentMaxValue(float maxValue)
    {
        m_maxValue = maxValue;
        
    }
#endregion

    protected virtual void valueUpdate () {
		sliderBar.value = valuePercent ();
	}

    protected float valuePercent()
	{
        Debug.Log((float)m_value / (float)m_maxValue);
		return (float)m_value / (float)m_maxValue;
	}

    /// <summary>
    /// Call this function to damage health UI. 
    /// Recommended: use a SendMessage function
    /// </summary>
    /// <param name="damage">Damage.</param>
    protected void ReduceValue(float value)
	{
		StartCoroutine ("UpdateValue", -value);
	}

    protected void AddValue(float value)
    {
        StartCoroutine("UpdateValueUp", value);
    }

    protected IEnumerator UpdateValue(float damage)
	{
        
		float updatedValue = m_value + damage;
		if (updatedValue < 0)
			updatedValue = 0;


        while (m_value > updatedValue ) {
			m_value = (int)Mathf.Lerp (m_value, updatedValue, Time.deltaTime);
            valueUpdate ();
			yield return null;
		}
	}

    protected IEnumerator UpdateValueUp(float restore)
    {

        float updatedValue = m_value + restore;
        if (updatedValue < 0)
            updatedValue = 0;

        
        while (m_value < updatedValue)
        {
            m_value = Mathf.CeilToInt(Mathf.Lerp(m_value, updatedValue, Time.deltaTime));
            valueUpdate();
            yield return null;
        }
    }
}
