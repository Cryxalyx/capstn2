﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Damage tester. 
/// </summary>
public class DamageTester : MonoBehaviour {

	public GameObject player, boss;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0)) {
            player.GetComponent<Unit>().TakeDamage(10);
        } /*else if (Input.GetMouseButtonDown (1)) {
			boss.SendMessage ("TakeDamage", 30);
		}*/
    }
		
	public void Damage(int damage)
	{
		player.SendMessage ("TakeDamage", damage);
	}
}
