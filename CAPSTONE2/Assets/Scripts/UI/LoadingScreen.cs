﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour {

    public Text LoadingText;
    bool loading;

	// Use this for initialization
	void Awake () {
        SingletonManager.Register(this);
        gameObject.SetActive(false);
	}

    public void Show()
    {
        gameObject.SetActive(true);
        loading = true;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        loading = false;
    }

    private void Update()
    {
        if(loading)
            LoadingText.color = new Color(LoadingText.color.r, LoadingText.color.g, LoadingText.color.b, Mathf.PingPong(Time.time, 1)); 
    }
}
