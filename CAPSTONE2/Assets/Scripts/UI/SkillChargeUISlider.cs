﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillChargeUISlider : UISlider {

    void Awake()
    {
        UIUpdater.StartListening("ChargeSetUp", ChargeSetUp);
        UIUpdater.StartListening("MaxChargeSetUp", MaxChargeSetUp);
    }
    // Use this for initialization
    void Start()
    {
        sliderBar.value = valuePercent();
    }

    private void OnEnable()
    {
        UIUpdater.StartListening("UseCharge", ReduceValue);
        UIUpdater.StartListening("RestoreCharge", AddValue);
        UIUpdater.StartListening("UpdateCharge", UpdateCharge);
        UIUpdater.StartListening("UpdateMaxCharge", UpdateMaxCharge);
    }

    private void OnDisable()
    {
        UIUpdater.StopListening("UseCharge", ReduceValue);
        UIUpdater.StopListening("RestoreCharge", AddValue);
        UIUpdater.StopListening("UpdateCharge", UpdateCharge);
        UIUpdater.StopListening("UpdateMaxCharge", UpdateMaxCharge);
    }

    void UpdateCharge(float charge)
    {
        m_value = charge;
        sliderBar.value = valuePercent();
    }

    void UpdateMaxCharge(float maxCharge)
    {
        m_maxValue = maxCharge;
        sliderBar.value = valuePercent();
    }

    void ChargeSetUp(float value)
    {
        Debug.Log(value);
        m_value = value;
        sliderBar.value = valuePercent();
        UIUpdater.StopListening("ChargeSetUp", ChargeSetUp);
    }

    void MaxChargeSetUp(float maxValue)
    {
        m_maxValue = maxValue;
        UIUpdater.StopListening("MaxChargeSetUp", MaxChargeSetUp);
    }

    protected override void valueUpdate()
    {
        base.valueUpdate();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
