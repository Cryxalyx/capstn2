﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CoolDownUI : MonoBehaviour {

    public List<GameObject> Icons;
    GameObject m_currentIcon;
    float m_currentCooldown;
    int m_currentIconIndex;
    private void Start()
    {
        UIUpdater.StartListening("UpdateIcon", SelectCooldownIcon);
    }

    private void OnEnable()
    {
        UIUpdater.StartListening("UpdateIcon", SelectCooldownIcon);
    }

    private void OnDisable()
    {
        UIUpdater.StopListening("UpdateIcon", SelectCooldownIcon);
    }

    private void OnDestroy()
    {
        UIUpdater.StopListening("UpdateIcon", SelectCooldownIcon);
    }

    void SelectCooldownIcon(float index)
    {
        m_currentIconIndex = (int)index;
        SwitchIcon();
    }

    void SwitchIcon()
    {
        if(m_currentIcon != null)
            m_currentIcon.gameObject.SetActive(false);
        m_currentIcon = Icons[m_currentIconIndex];
        m_currentIcon.gameObject.SetActive(true);
    }

}
