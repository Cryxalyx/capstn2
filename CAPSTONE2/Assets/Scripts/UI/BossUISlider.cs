﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossUISlider : UISlider {

    void Awake()
    {
        UIUpdater.StartListening("BossHealthSetUp", ValueSetUp);
    }
    // Use this for initialization
    void Start()
    {

    }
    /// <summary>
    /// Called on Awake or Starts via events.
    /// </summary>
    /// <param name="value">base value</param>
    void ValueSetUp(float value)
    {
        m_value = value;
        m_maxValue = m_value;
        UIUpdater.StopListening("BossHealthSetUp", ValueSetUp);
    }

    private void OnEnable()
    {
        UIUpdater.StartListening("BossTakeDamage", ReduceValue);
        UIUpdater.StartListening("BossRestoreHealth", AddValue);
    }

    private void OnDisable()
    {
        UIUpdater.StopListening("BossTakeDamage", ReduceValue);
        UIUpdater.StopListening("BossRestoreHealth", AddValue);
    }

    protected override void valueUpdate()
    {
        base.valueUpdate();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
