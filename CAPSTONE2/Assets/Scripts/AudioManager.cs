﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioSource LevelAudioSource
    {
        get
        {
            return levelAudioSource;
        }
    }
    private AudioSource levelAudioSource;
	// Use this for initialization
	void Awake () {
        SingletonManager.Register(this);
	}

    public void SetLevelAudioSource(AudioSource audioSource)
    {
        levelAudioSource = audioSource;
    }
}
