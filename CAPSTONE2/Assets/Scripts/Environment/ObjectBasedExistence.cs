﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBasedExistence : MonoBehaviour {

    public GameObject Protectee;
        
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Protectee == null)
            Destroy(gameObject);
	}
}
