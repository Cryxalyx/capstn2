﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlatformModifier : MonoBehaviour {
    /// <summary>
    /// Function that holds what the modifications of the platforms are.
    /// </summary>
    public abstract void ModifyPlatform();
}
