﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class NextSceneTransition : MonoBehaviour {

    public int nextSceneIndex;

    public void NextScene()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene(nextSceneIndex);
    }
}
