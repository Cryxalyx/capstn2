﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentDmgTirgger : MonoBehaviour {


    public bool m_HasPushForce;
    [SerializeField] bool damaging;
    [SerializeField] float damage;
    [SerializeField] float forceMultiplier;
    [SerializeField] Vector3 m_direction;
    // Use this for initialization
    void Start()
    {

    }

    public bool IsDamaging
    {
        get { return damaging; }
    }

    Vector3 PushTowards(Vector3 direction)
    {
        Vector3 newForce;
        if (m_direction.magnitude != 0)
            return newForce = m_direction;

        newForce = direction;

        return newForce;
    }

    void AddForce(MonoBehaviour mb)
    {
        mb.GetComponent<Rigidbody>().velocity = Vector3.zero;
        mb.GetComponent<Transform>().SetParent(null);
        Vector3 direction = (mb.GetComponent<Transform>().position - transform.position) * forceMultiplier;
        direction.y = 0;
        mb.GetComponent<Rigidbody>().AddForce(PushTowards(direction) * forceMultiplier, ForceMode.Impulse);
    }

    void OnTriggerStay(Collider other)
    {
        if (!damaging)
            return;
        MonoBehaviour[] monos = other.GetComponents<MonoBehaviour>();
        foreach(MonoBehaviour mb in monos)
        {
            if(mb is IDamageable)
            {
                IDamageable dmg = (IDamageable)mb;
                dmg.TakeDamage(damage);
                if (m_HasPushForce)
                    AddForce(mb);
            }
        }
    }
    public void SetDamaging(bool doDamage)
    {
        damaging = doDamage;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
