﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagingDebrisVisual : GroundVisual {

    private void OnDestroy()
    {
        GetComponent<GroundImpactDamager>().DealImpactDamage();
    }

}
