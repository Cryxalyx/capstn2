﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attach this to Object of Visual Feedback of ground targeteing.
/// </summary>
public class GroundImpactDamager : MonoBehaviour {

    public GameObject m_OnDestoryParticleSystem;
    public bool m_DestroyOrigin;

    public virtual void DealImpactDamage()
    {
        float radius = 4;
        if (GetComponent<GroundVisual>() != null)
            radius = GetComponent<GroundVisual>().GetOverlapSize;
        Collider[] cols = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider col in cols)
        {
            MonoBehaviour[] mbs = col.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour mb in mbs)
            {
                if (mb is IDamageable && mb.GetComponent<PlayerUnit>() != null)
                {
                    IDamageable dmg = (IDamageable)mb;
                    dmg.TakeDamage(20);

                    //Apply force upon impact;
                    Vector3 direction = mb.GetComponent<Transform>().position - transform.position;
                    direction.y = 2;
                    mb.GetComponent<Rigidbody>().AddForce(direction * 2,ForceMode.Impulse);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == GetComponent<GroundVisual>().m_OriginalObjectSpawner)
        {
            Instantiate(m_OnDestoryParticleSystem, this.transform.position, Quaternion.identity);
            DealImpactDamage();
            if(m_DestroyOrigin)
                Destroy(GetComponent<GroundVisual>().m_OriginalObjectSpawner);
        }
            
    }
    //Help Visualize Overlap sphere.
    private void OnDrawGizmosSelected()
    {
        float radius = 4;
        if (GetComponent<GroundVisual>() != null)
            radius = GetComponent<GroundVisual>().GetOverlapSize;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
