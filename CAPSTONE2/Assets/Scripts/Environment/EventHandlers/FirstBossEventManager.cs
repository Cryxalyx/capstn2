﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class BattleEvent : UnityEvent
{

}

public class FirstBossEventManager : MonoBehaviour {

    private Dictionary<string, BattleEvent> eventDictionary;

    private static FirstBossEventManager BattleUpdater;

    public static FirstBossEventManager instance
    {
        get
        {
            if (!BattleUpdater)
            {
                BattleUpdater = FindObjectOfType(typeof(FirstBossEventManager)) as FirstBossEventManager;

                if (!BattleUpdater)
                {
                    Debug.LogError("There needs to be one active FirstBossEventManager script on a GameObject in your scene.");
                }
                else
                {
                    BattleUpdater.Init();
                }
            }

            return BattleUpdater;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, BattleEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction listener)
    {
        BattleEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new BattleEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction listener)
    {
        if (BattleUpdater == null) return;
        BattleEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        BattleEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }
}
