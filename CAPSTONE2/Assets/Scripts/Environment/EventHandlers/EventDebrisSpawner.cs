﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventDebrisSpawner : Spawner {

    public GameObject Debris;
    public int SpawnNumber;
    public string EventName;
	// Use this for initialization
	void Start () {
        LevelEventHandler.StartListening(EventName, MassSpawn);
	}
	
    void MassSpawn(){
        for(int i = 0; i < SpawnNumber; i++)
        {
            Spawn(Debris);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, m_radius);
    }
}
