﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class NextLevelTrigger : MonoBehaviour
{
    public string NextLevelName;
    public int NextLevelInt;

    public void LoadNextScene()
    {
        if (NextLevelName == null)
            NextLevelName = SceneManager.GetSceneAt(NextLevelInt).name;
        GameInstance.LoadGameMode(NextLevelName);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerUnit>() == null)
            return;
        LoadNextScene();
    }
}
