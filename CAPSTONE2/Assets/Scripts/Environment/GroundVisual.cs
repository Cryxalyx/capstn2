﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GroundVisual : MonoBehaviour {

    public GameObject m_MaxVisualObject;
    public GameObject m_ResizableVisualObject;
    public float m_SizeMultiplier;
    public Direction m_MaxRotation;
    public Direction m_ResizableRotation;

    public GameObject m_OriginalObjectSpawner;
    protected float m_reValue;
    protected float m_yDefault;
    protected float m_maxSize;
    protected float m_visualMaxSize;
    // Use this for initialization
    void Start() {
        m_yDefault = transform.localScale.y;
        m_maxSize = m_OriginalObjectSpawner.transform.localScale.x;
        m_visualMaxSize = m_MaxVisualObject.transform.localScale.x;
        SetMaxVisual();
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public float GetMaxSize()
    {
        return  m_maxSize * (m_OriginalObjectSpawner.transform.localScale.x);
    }

    public float GetOverlapSize
    {
        get { return GetMaxSize() - (GetMaxSize()/ 8); }
    }

    public void SetOrigin(GameObject origin)
    {
        m_OriginalObjectSpawner = origin;
    }

    protected void SetMaxVisual()
    {
        float maxSize = m_MaxVisualObject.transform.localScale.x * (GetMaxSize() * m_SizeMultiplier + (GetMaxSize()/m_SizeMultiplier));
        m_MaxVisualObject.transform.localScale = new Vector3(maxSize, m_MaxVisualObject.transform.localScale.y, maxSize);
    }

    protected virtual void ReSize()
    {
        float maxSize = GetMaxSize() * m_maxSize;
        float distance = Mathf.Abs((Vector3.Distance(m_ResizableVisualObject.transform.position, m_OriginalObjectSpawner.transform.position) - 1));
        m_reValue = maxSize - distance;
        m_reValue = Mathf.Clamp(m_reValue, 0, maxSize);
        m_ResizableVisualObject.transform.localScale = new Vector3(m_reValue, m_ResizableVisualObject.transform.localScale.y, m_reValue);
    }

    protected virtual void ReColor()
    {
            GetComponentInChildren<SpriteRenderer>().color = new Color(m_reValue*2, 1 -m_reValue, 1 - m_reValue);    
    }

    protected void RotateVisuals()
    {
        m_ResizableVisualObject.transform.Rotate(Vector3.up, 2 * (int)m_MaxRotation);
        m_MaxVisualObject.transform.Rotate(Vector3.up, 3 * (int)m_ResizableRotation);
    }

    protected void FixedUpdate()
    { 
        if (m_OriginalObjectSpawner == null)
        {
            Destroy(gameObject);
            return;
        }
            
        ReSize();
        RotateVisuals();
        //ReColor();
    }
}
