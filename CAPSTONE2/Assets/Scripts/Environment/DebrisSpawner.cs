﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisSpawner : Spawner {

    public List<GameObject> Debris;
    int index;
    float m_spawnTimer;
    float spawnTime;
    /// <summary>
    /// Spawns the debris from spawner location. Made virtual to change behavior of spawning.
    /// </summary>
    /// <param name="index"> index based on size of the list of debris</param>
    public override void Spawn(GameObject spawn)
    {
        index = Random.Range(0, Debris.Count);
        base.Spawn(spawn);
        m_spawnTimer = 0;
        spawnTime = RandomSpawning();
    }

    public float RandomSpawning()
    {
        return spawnTime = (Random.Range(m_MinSpawnTime, m_MaxSpawnTime));
    }
    bool ShouldSpawn()
    {
        m_spawnTimer += Time.deltaTime;
        return (m_spawnTimer > spawnTime);
    }

    private void Update()
    {
        if (ShouldSpawn())
            Spawn(Debris[index]);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, m_radius);
    }

}
