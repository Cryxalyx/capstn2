﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level0GearEffect : MonoBehaviour {

    public ParticleSystem ps;
    ParticleSystem.EmissionModule pse;
    private void Start()
    {
        pse = ps.emission;
    }

    void ActivateEffect()
    {
        
        pse.enabled = GetComponent<EnvironmentDmgTirgger>().IsDamaging;

    }

    private void Update()
    {
        ActivateEffect();
    }
}
