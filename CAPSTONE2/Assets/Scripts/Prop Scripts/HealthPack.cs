﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : Consumables, IAbsorbable {

    public float m_Value;
    float m_value;
    public override void ApplyEffect(Unit unit)
    {
        m_value = m_Value;
        if (unit.Health > unit.MaxHealth - m_Value)
			m_value = (unit.MaxHealth - unit.Health);
        unit.AddHealth(m_value);
        UIUpdater.TriggerEvent("RestoreHealth", m_value);
		Destroy(gameObject);
	}
    

    private void OnTriggerEnter(Collider other)
    {
		if (other.GetComponent<Unit> () != null) {
			ApplyEffect (other.GetComponent<Unit> ());   
		}
    }
}
