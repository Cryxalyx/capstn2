﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectExistanceDisabler : MonoBehaviour {

    public GameObject ObjectToEnable;
    bool m_enable;
    private void Update()
    {
        if (GetComponent<CharacterControl>().IsDead)
            m_enable = true;
    }

    private void OnDisable()
    {
        if (m_enable)
            ObjectToEnable.SetActive(true);
    }
}
