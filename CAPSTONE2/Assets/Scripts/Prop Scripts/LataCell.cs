﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LataCell : MonoBehaviour, IBreakable {

    public void Break()
    {
        Destroy(gameObject);
    }
}
