﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powercell : MonoBehaviour,IBreakable {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Break()
    {
        LevelEventHandler.TriggerEvent("PowerCellOut");
        Destroy(gameObject);
    }
}
