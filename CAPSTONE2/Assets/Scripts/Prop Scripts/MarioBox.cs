﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioBox : MonoBehaviour {

	public GameObject mysteryObject; // objects that you wanted to show
    public Vector3 PushForce;
    public bool OneTimeUse;
	public bool isCenteredPivot; // is the pivot of mysteryObject is centered?
	Animator anim; //Animator of Box

	void Start() {
		anim = this.GetComponent<Animator> ();
	}

	void OnCollisionEnter(Collision col) {
        //Check if its a player and hit the bottom face of the box
        if (col.gameObject.GetComponent<PlayerUnit>() != null && (col.collider.transform.position.y - col.collider.transform.localScale.y) < (this.transform.position.y - this.transform.localScale.y))
            RevealMysteryObject();

    }

	//Immediately called right after the animation of collide
	//Check the animation tab, there should be an event at the end that calls this function
	public void RevealMysteryObject() {
		GameObject tmp = Instantiate (mysteryObject, this.transform.position + Vector3.up, Quaternion.identity);
        tmp.GetComponent<Rigidbody>().AddForce(PushForce * 100, ForceMode.Impulse);
        if (OneTimeUse)
        {
            Destroy(gameObject);
        }
		//tmp.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		//StartCoroutine (ScalingSize (Vector3.one * 0.5f, tmp, isCenteredPivot));
	}

	IEnumerator ScalingSize(Vector3 limitSize, GameObject obj, bool isPivotCenter) {
		Vector3 tmp = obj.transform.position + new Vector3(0.0f, 0.5f);
		while (obj.transform.localScale.magnitude < limitSize.magnitude) {
			Material mat = obj.GetComponent<Renderer> ().material;
			DynamicColor (mat);
			obj.transform.localScale = Vector3.Lerp (obj.transform.localScale, limitSize, Time.deltaTime);
			if (isPivotCenter)
				obj.transform.position = Vector3.Lerp (obj.transform.position, tmp, Time.deltaTime);
			yield return null;
		}
	}

	//Dynamic Color
	void DynamicColor(Material mat) {
		Color tempColor = mat.color;
		tempColor.r += 0.01f;
		tempColor.g += 0.02f;
		tempColor.b += 0.35f;

		if (tempColor.r > 1)
			tempColor.r = 0f;
		if (tempColor.g > 1)
			tempColor.g = 0f;
		if (tempColor.b > 1)
			tempColor.b = 0;

		mat.color = tempColor;
	}
}
