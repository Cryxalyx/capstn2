﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEffect : MonoBehaviour {

    ParticleSystem[] pss;
	// Use this for initialization
	void Start () {
        pss = GetComponentsInChildren<ParticleSystem>();
	}
	
    bool IsDone()
    {
        foreach(ParticleSystem ps in pss)
        {
            if (ps.IsAlive())
            {
                return false;
            }
        }
        return true;
    }

	// Update is called once per frame
	void Update () {
        if (IsDone())
        {
            Destroy(gameObject);
        }
	}
}
