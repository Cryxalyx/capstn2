﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName ="GadgetDataObject/GadgetData")]
public class GadgetData : ScriptableObject {

    public Sprite Icon;
    public string Name;
    public string Description;
    public string UniqueSkill;
    public int Damage;
    public int Cost;

}
