﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Unit : MonoBehaviour, IDamageable {

    public AudioSource Damaged;

    public float BaseHealth;
    public float MaximumHealth;
    public float InvulnerablityTime;

    protected bool m_dead;
    protected float m_maxHealth;
    protected float m_baseHealth;
    protected float m_disableObjectTimer;
    float invulTimer;
    [SerializeField]protected bool m_invulnerable;

    public float Health
    {
        get { return m_baseHealth; }
    }

    public float MaxHealth
    {
        get { return m_maxHealth; }
    }

    /// <summary>
    /// Returns True when the invulnerability time runs out.
    /// </summary>
    /// <returns></returns>
    public virtual bool Recovered()
    {
        invulTimer += Time.deltaTime;
        if (invulTimer > InvulnerablityTime)
        {
            invulTimer = 0;
            return true;
        }
        return false;
    }

	// Use this for initialization
	protected virtual void Start () {
        m_baseHealth = BaseHealth;
        
        if (MaximumHealth == 0)
            m_maxHealth = BaseHealth;
        else
            m_maxHealth = MaximumHealth;
        MaximumHealth = m_maxHealth;
	}
	
    /// <summary>
    /// Triggers Character Controller Death to disable movement.
    /// </summary>
    public virtual void Die()
    {
        GetComponent<CharacterControl>().Die();
    }

    /// <summary>
    /// Confirms this Character is Dead.
    /// </summary>
    public void Dead()
    {
        m_dead = true;
    }

    /// <summary>
    /// Timer for Deleting Object from scene.
    /// </summary>
    /// <returns></returns>
    protected virtual bool Disappear()
    {
        m_disableObjectTimer += Time.deltaTime;
        return (m_disableObjectTimer > 5);
    }

    /// <summary>
    /// Function used to receive external Damage.
    /// </summary>
    /// <param name="damage"></param>
    public virtual void TakeDamage(float damage) 
    {
        if (Damaged != null)
            Damaged.Play();
        m_baseHealth -= damage;
    }

    public void AddHealth(float value)
    {
        m_baseHealth += value;
        if (m_baseHealth > m_maxHealth)
            m_baseHealth = m_maxHealth;
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
