﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float m_radius;
    public float m_MinSpawnTime;
    public float m_MaxSpawnTime;

    /// <summary>
    /// Spawns the debris from spawner location. Made virtual to change behavior of spawning.
    /// </summary>
    /// <param name="index"> index based on size of the list of debris</param>
    public virtual void Spawn(GameObject spawn)
    {
        Instantiate(spawn, RandomSpawnPoint(), Quaternion.identity);
    }

    /// <summary>
    /// Calculates spawnpoint based on radius provided. Made virtual to change computation for spawnpoint.
    /// </summary>
    /// <returns></returns>
    public virtual Vector3 RandomSpawnPoint()
    {
        float x = Random.Range(-m_radius, m_radius);
        float z = Random.Range(-m_radius, m_radius);
        Vector3 spwnPoint = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
        return spwnPoint;
    }
}
