﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumables : MonoBehaviour {

    public abstract void ApplyEffect(Unit unit);
}
