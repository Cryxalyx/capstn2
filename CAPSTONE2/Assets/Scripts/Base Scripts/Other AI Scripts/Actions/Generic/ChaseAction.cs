﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Actions/Chase Action")]
public class ChaseAction : AiAction {

    public override void Act(StateController controller)
    {
        Chase(controller);
    }

    Vector3 Direction(StateController controller)
    {
        Vector3 distance = controller.m_ChaseTarget.position - controller.transform.position;
        if(Intercept(Vector3.Distance(controller.m_ChaseTarget.position, controller.transform.position)))
        {
            return (distance + (controller.m_ChaseTarget.GetComponent<Rigidbody>().velocity * Time.deltaTime * 100)) * 10;
        }      
        return distance;
    }

    bool Intercept(float distance)
    {
        return (distance > 5);    
    }
    void Chase(StateController controller)
    {
        if(controller.m_ChaseTarget != null)
        {

            controller.CharControl.Move(Direction(controller).normalized, false, false);
            controller.CharControl.RotateCharacter(Direction(controller).normalized * 0.1f);
        }
    }
}
