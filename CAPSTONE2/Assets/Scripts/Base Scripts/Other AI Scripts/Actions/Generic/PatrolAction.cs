﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Actions/Patrol")]
public class PatrolAction : AiAction {

    public override void Act(StateController controller)
    {
        Patrol(controller);
    }

    void Patrol(StateController controller)
    {
        Vector3 direction = controller.wayPointList[controller.nextWayPoint].position - controller.transform.position ;
        controller.CharControl.Move(direction.normalized, false, false);
        controller.CharControl.RotateCharacter(direction.normalized);
        Vector3 waypoint = controller.wayPointList[controller.nextWayPoint].position;
        waypoint.y = 0;
        Vector3 mypos = controller.transform.position;
        mypos.y = 0;
        if (Vector3.Distance(mypos, waypoint) < 1f)
        {
            controller.nextWayPoint = Random.Range(0, controller.wayPointList.Count) % controller.wayPointList.Count;
        }
    }
}
 