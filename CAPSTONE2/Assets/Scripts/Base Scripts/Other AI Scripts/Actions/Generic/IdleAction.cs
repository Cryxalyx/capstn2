﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Actions/Idle")]
public class IdleAction : AiAction {

    public override void Act(StateController controller)
    {
        Idle(controller);
    }

    void Idle(StateController controller)
    {
        controller.CharControl.Move(Vector3.zero, false, false);
        controller.CharControl.RotateCharacter(Vector3.zero);
    }
}
