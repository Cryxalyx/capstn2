﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Actions/Shoot Action")]
public class ShootAction : AiAction {

    public override void Act(StateController controller)
    {
        Shoot(controller);
    }

    void Shoot(StateController controller)
    {
        Vector3 direction = controller.GetComponent<DamagingUnit>().target.transform.position - controller.transform.position;
        direction.y = 0;
        controller.CharControl.RotateCharacter(direction.normalized);
        if (controller.GetComponent<BullEBlasterController>().Attack || controller.GetComponent<BullEBlasterController>().IsCoolDown)
            return;
        int random = Random.Range(0, 2);
        controller.GetComponent<BullEBlasterController>().AttackArm = random + 1;
        controller.GetComponent<BullEBlasterController>().Attack = true;
        controller.GetComponent<BullEBlasterController>().Shoot();
    }
}
