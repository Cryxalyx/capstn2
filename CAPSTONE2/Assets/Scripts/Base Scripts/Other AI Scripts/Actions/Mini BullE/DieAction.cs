﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Actions/Die Action")]
public class DieAction : AiAction {

    public override void Act(StateController controller)
    {
        
        Die(controller);
    }

    void Die(StateController controller)
    {

        if (controller.CharControl.IsDead)
            return;
        controller.CharControl.Move(Vector3.zero, false, false);
        controller.gameObject.GetComponent<Unit>().Die();
    }
}
