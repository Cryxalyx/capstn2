﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedAction : AiAction {

    public override void Act(StateController controller)
    {
        Damaged(controller);
    }

    void Damaged(StateController controller)
    {
        //controller.CharControl.Move(Vector3.zero, false, false);
        controller.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
