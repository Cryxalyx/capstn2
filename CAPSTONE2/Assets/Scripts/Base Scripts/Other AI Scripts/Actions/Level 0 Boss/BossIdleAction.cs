﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Actions/Idle Action")]
public class BossIdleAction : AiAction {

    public override void Act(StateController controller)
    {
        Idle(controller);
    }

    protected virtual void Idle(StateController controller)
    {
        
    }
}
