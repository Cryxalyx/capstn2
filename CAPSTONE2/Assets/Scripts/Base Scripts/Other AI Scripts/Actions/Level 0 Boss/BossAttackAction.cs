﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Actions/BossAttackAction")]
public class BossAttackAction : AiAction {

    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    protected virtual void Attack(StateController controller)
    {

        BossBehavior bb = controller.GetComponent<BossBehavior>();
        if (bb.CurrentAttackPattern != null)
            return;
        controller.GetComponent<BossBehavior>().Attack();
    }
}
