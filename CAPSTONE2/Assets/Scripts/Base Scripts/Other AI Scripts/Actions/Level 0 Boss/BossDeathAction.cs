﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Actions/Death Action")]
public class BossDeathAction : AiAction {
    public float DisableCounter;
    public override void Act(StateController controller)
    {
        Die(controller);
    }

    public void Die(StateController controller)
    {
        if (controller.CheckIfCountDownElapsed(DisableCounter))
            controller.gameObject.SetActive(false);
        if (controller.CharControl.IsDead)
            return;
        controller.CharControl.Move(Vector3.zero, false, false);
        controller.gameObject.GetComponent<Unit>().Die();
    }
}
