﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Decisions/Shoot Decision")]
public class ShootDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Shoot(controller);
    }

    bool Shoot(StateController controller)
    {
        if (controller.gameObject.GetComponent<BullEBlasterController>().IsCoolDown == false)
        {
            controller.CharControl.Move(Vector3.zero, false, false);
            return true;
        }
        return false;
    }
}
