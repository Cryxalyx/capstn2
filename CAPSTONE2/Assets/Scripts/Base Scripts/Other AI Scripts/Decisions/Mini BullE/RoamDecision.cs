﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Decisions/Roam Decision")]
public class RoamDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Roam(controller);
    }

    bool Roam(StateController controller)
    {
        return (controller.GetComponent<BullEBlasterController>().IsCoolDown);
    }
}
