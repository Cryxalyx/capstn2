﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Decisions/Damaged Decision")]
public class DamagedDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Damaged(controller);
    }

    bool Damaged(StateController controller)
    {
        return (controller.CharControl.IsDamaged);
    }
}
