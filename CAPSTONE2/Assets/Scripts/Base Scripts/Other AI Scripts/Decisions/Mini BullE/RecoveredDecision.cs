﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Decisions/Recovered Decision")]
public class RecoveredDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Recovered(controller);
    }

    bool Recovered(StateController controller)
    {
        return (!controller.CharControl.IsDamaged);
    }
}
