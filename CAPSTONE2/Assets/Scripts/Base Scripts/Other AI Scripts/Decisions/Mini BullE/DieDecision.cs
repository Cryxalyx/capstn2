﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/MiniBullE/Decisions/Die Decision")]
public class DieDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return (controller.GetComponent<Unit>().Health <= 0 && !controller.CharControl.IsDead);
    }
}
