﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Decisions/Attack Decision")]
public class BossAttackDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Attack(controller);
    }

    bool Attack(StateController controller)
    {
        return controller.CheckIfCountDownElapsed(controller.GetComponent<BossBehavior>().AttackTransitionTime);
    }
}
