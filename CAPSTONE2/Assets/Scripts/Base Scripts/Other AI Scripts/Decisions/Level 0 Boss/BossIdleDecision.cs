﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Decisions/Idle Decision")]
public class BossIdleDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Idle(controller);
    }
    bool Idle(StateController controller)
    {
        BossBehavior bb = controller.GetComponent<BossBehavior>();
        if (bb.CurrentAttackPattern.IsFinished)
        {
            bb.CurrentAttackPattern.EndAttack();
            bb.CurrentAttackPattern = null;
            controller.CharControl.Move(Vector3.zero, false, false);
            return true;
        }
        return false;
    }
}
