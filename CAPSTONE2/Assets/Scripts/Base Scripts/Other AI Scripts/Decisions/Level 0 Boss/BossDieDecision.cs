﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Boss/Decisions/Die Decision")]
public class BossDieDecision : Decision {

    public override bool Decide(StateController controller)
    {
        if(controller.GetComponent<BossBehavior>().Health <= 0)
        {
            controller.gameObject.GetComponent<Unit>().Die();
            LevelEventHandler.TriggerEvent("BossDeath");
            GameObject ees = Instantiate(controller.GetComponent<BossBehavior>().DeathEffect, controller.GetComponent<BossBehavior>().DeathEffectSpawnPoint.position, Quaternion.identity);
            ees.GetComponent<Transform>().SetParent(controller.GetComponent<BossBehavior>().DeathEffectSpawnPoint);
            
            return true;
        }
        return false;
    }
}
