﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Decisions/Look Decision")]
public class LookDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool targetInSight = Look(controller);
        return targetInSight;
    }
    bool Look(StateController controller)
    {
        RaycastHit hit;
        if (Physics.SphereCast(controller.eyes.position, 0.3f, controller.eyes.forward, out hit, 4)
            && hit.collider.gameObject.GetComponent<Unit>() != null)
        {
            controller.m_ChaseTarget = hit.transform;
            return true;
        }
        return false;
    }

    
}
