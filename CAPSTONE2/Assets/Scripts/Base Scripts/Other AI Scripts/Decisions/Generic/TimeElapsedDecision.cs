﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Decisions/Time Elapsed Decision")]
public class TimeElapsedDecision : Decision {

    public float timeElapse;

    public override bool Decide(StateController controller)
    {
        return (controller.CheckIfCountDownElapsed(timeElapse));
    }
}
