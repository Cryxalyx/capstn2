﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Decisions/Cease Pursuit")]
public class CeasePursuitDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Cease(controller);
    }

    bool Cease(StateController controller)
    {
        if (controller.m_ChaseTarget == null)
            return false;
        if (Vector3.Distance(controller.m_ChaseTarget.position, controller.transform.position) > 10)
        {
            controller.m_ChaseTarget = null;
            return true;
        }
        return false;
    }
}
