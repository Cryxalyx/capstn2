﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/Decisions/Stop Decision")]
public class StopDecision : Decision {

    public override bool Decide(StateController controller)
    {
        return Stop(controller);
    }

    public bool Stop(StateController controller)
    {
        return controller.CheckIfCountDownElapsed(5);
         
    }

}
