﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleProjectile : Projectiles {



    public override void SetTarget(GameObject Target)
    {
        base.SetTarget(Target);
    }

    public override void SetTarget(Vector3 target)
    {
        base.SetTarget(target);
    }
    // Update is called once per frame
    void Update ()
    {
	    if(targetPosition != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }

        if(transform.position == targetPosition)
        {
            //DestroyObject(this.gameObject);
        }
	}
}
