﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : Projectiles {

    public float m_ClosestRange;
    protected Vector3 direction;
    protected bool m_closestRange;
    void Update()
    {
        if (Vector3.Distance(target.transform.position, this.transform.position) < m_ClosestRange)
            m_closestRange = true;

        if (target == null)
            return;
        GetComponent<Rigidbody>().velocity = Direction() * speed;
    }

    protected virtual Vector3 Direction()
    {

        if (Vector3.Distance(target.transform.position, this.transform.position) > m_ClosestRange && !m_closestRange)
        {
            
            direction = target.transform.position - this.transform.position;
            direction.y = 0;
        }
        

        return direction;
    }

}
