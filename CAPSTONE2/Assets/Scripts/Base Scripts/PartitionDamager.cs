﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartitionDamager : MonoBehaviour, IDamageable {

    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeDamage(float damage)
    {
        GetComponentInParent<Unit>().TakeDamage(damage);
    }
}
