﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletInstancer : MonoBehaviour, IAbsorbable {

    [SerializeField] protected string path;
    public virtual GameObject GetReference()
    {
        return (GameObject)Resources.Load(path);
    }

    void Absorbed(BlasterControl bc)
    {
        bc.AddGadget(GetReference());
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.GetComponent<BlasterControl>() != null)
        {
            Absorbed(other.gameObject.GetComponent<BlasterControl>());
        }
    }

    void Update()
    {
        transform.Rotate(Vector3.up.normalized);
    }

}
