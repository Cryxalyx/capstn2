﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectiles : MonoBehaviour {


    public Vector3 targetPosition;
    public GameObject target;
    public float speed;


    public virtual void SetTarget(Vector3 target)
    {
        targetPosition = target;
    }
    public virtual void SetTarget(GameObject Target)
    {
        target = Target;
    }

}
