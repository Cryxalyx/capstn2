﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable/State")]
public class State : ScriptableObject {

    public AiAction[] actions;
    public Transition[] transitions;

    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    /// <summary>
    /// Function to do all actions listed/Given to AI.
    /// </summary>
    /// <param name="controller"></param>
    void DoActions(StateController controller)
    {
        if (actions.Length == 0)
            return;
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    /// <summary>
    /// Checks for conditions to transition to Another State.
    /// </summary>
    /// <param name="controller"></param>
    void CheckTransitions(StateController controller)
    {
        if (transitions.Length == 0)
            return;
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
