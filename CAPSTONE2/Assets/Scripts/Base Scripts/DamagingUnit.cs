﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagingUnit : Unit {


    [SerializeField]float m_BaseDamage;
    public GameObject target;
    float m_baseDamage;

    /// <summary>
    /// Use this to get base damage of any damaging unit.
    /// </summary>
    public float Damage
    {
        get { return m_baseDamage; }
        set { m_baseDamage = value; }
    }

    public float BaseDamage
    {
        get { return m_BaseDamage; }
    }

    public void RestoreDamage()
    {
        m_baseDamage = BaseDamage;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        m_baseDamage = BaseDamage;
        
    }
}
