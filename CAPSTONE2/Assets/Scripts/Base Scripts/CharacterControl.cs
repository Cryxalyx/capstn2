﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterControl : MonoBehaviour {


    
    [Range(1, 10)]
    public float m_JumpPower;
    /// <summary>
    /// The higher the number the faster the player moves;
    /// </summary>
    public float m_MovementMultiplier;
    public float m_GravityMultiplier;
    public float m_TurnSpeed;
    public float m_StationaryTurnSpeed;
    public float m_GroundedDistance;

    //
    protected Animator m_animator;
    protected Rigidbody m_rigidBody;
    protected Vector3 m_groundNormal;
    protected Vector3 m_move;
    protected bool m_grounded;
    protected bool m_damaged;
    protected bool m_dead;
    protected float m_forwardAmount;
    protected float m_originalGroundedDistance;
    protected float m_sideMotion;
    protected float m_turnAmount;
    //
    
    //for Gizmo
    protected float hitdistance;


    // Use this for initialization
    void Awake () {
        m_rigidBody = GetComponent<Rigidbody>();
        m_rigidBody.velocity = Vector3.zero;
        m_grounded = true;
        m_animator = GetComponent<Animator>();
        if (m_GroundedDistance == 0)
            m_GroundedDistance = (transform.localScale.y / 3);

        m_originalGroundedDistance = m_GroundedDistance;
        if (m_MovementMultiplier <= 0)
            m_MovementMultiplier = 0.1f;

        
    }

#region public only

    public bool IsDamaged
    {
        get { return m_damaged; }
    }

    public bool IsDead
    {
        get { return m_dead; }
    }

    /// <summary>
    /// Used by outside scripts to check if player is grounded 
    /// </summary>
    /// <returns></returns>
    public bool IsGrounded()
    {
        return m_grounded;
    }

    /// <summary>
    /// Returns true once the damaged animation ends.
    /// </summary>
    /// <returns></returns>
    public bool HasRecovered()
    {
        return !m_damaged;
    }

    /// <summary>
    /// Sets the ground normal, sets whether or not the character is grounded, sets animation root, and sets possible parent platform.
    /// </summary>
    /// <param name="groundNormal"></param>
    /// <param name="grounded"></param>
    /// <param name="animatorRoot"></param>
    /// <param name="ground"></param>
    public void SetGrounded(Vector3 groundNormal, bool grounded, bool animatorRoot, Transform ground)
    {
        m_groundNormal = groundNormal;
        m_grounded = grounded;
        //m_animator.applyRootMotion = animatorRoot;

        if (ground == null)
        {
            transform.SetParent(ground);
            Quaternion reset = transform.localToWorldMatrix.rotation;
            reset.x = Quaternion.identity.x;
            reset.z = Quaternion.identity.z;
            transform.rotation = reset;
        }

        if (ground != null && ground.GetComponent<PlatformModifier>() != null)
        {
            transform.SetParent(ground);
        }
    }

    /// <summary>
    /// Used to Recover from Damage anim.
    /// </summary>
    public void Recover()
    {
        m_damaged = false;
        m_animator.SetFloat("DamageAmount", 0);
    }



    #endregion

#region Movement and Rotation.

    /// <summary>
    /// Used to Move the Character
    /// </summary>
    /// <param name="move"> is a computed vector3 velocity to be applied to the character.</param>
    /// <param name="crouch"> boolean to check if character is crouching or not</param>
    /// <param name="jump"> boolean to check if character is airborne or not</param>
    public virtual void Move(Vector3 move, bool crouch, bool jump)
    {


        //if (move.magnitude > 1f) move.Normalize();
        m_move = move;
        Vector3 tempMove = transform.InverseTransformDirection(move);
        CheckGrounded();
        move = Vector3.ProjectOnPlane(move,m_groundNormal);
        m_forwardAmount = tempMove.z;
        m_sideMotion = tempMove.x;

        if (!m_grounded)
        {
            ApplyGravity();
        }
        HandleMovement(jump);
        UpdateAnimator(move);
       

    }

    /// <summary>
    /// Handles players rotation based on a given Vector.
    /// </summary>
    /// <param name="move"> Vector three to which the model shall turn to</param>
    public virtual void RotateCharacter(Vector3 move)
    {
        

        //move_ad = move;

        //Copmputation for changes the Transforms directions so that the model rotates towards direction.
        if (move.magnitude > 1f) move.Normalize();
        Vector3 angularMovement = move;
        angularMovement = transform.InverseTransformDirection(angularMovement);
        angularMovement = Vector3.ProjectOnPlane(angularMovement, m_groundNormal);
        m_turnAmount = Mathf.Atan2(angularMovement.x, angularMovement.z);

        //m_forwardAmount = angularMovement.z;


        ApplyTurnAround();
    }


#endregion

#region overridables

    /// <summary>
    /// Sets the values for the parameters of the animator.
    /// </summary>
    /// <param name="move"></param>
    protected virtual void UpdateAnimator(Vector3 move)
    {
    }

    /// <summary>
    /// Function to hold any additional conditioning for changes in the movement of the character.
    /// </summary>
    /// <param name="jump"></param>
    protected virtual void HandleMovement(bool jump)
    { 
    }

    /// <summary>
    /// Handles movement while Airborne.
    /// </summary>
    /// <param name="velocity"></param>
    protected virtual void HandleAirMovement(Vector3 velocity)
    {
    }

    /// <summary>
    /// Used for triggering Damage animation.
    /// </summary>
    /// <param name="damagePercentage"></param>
    public virtual void TakenDamage(float damagePercentage)
    {
        m_damaged = true;
    }

    public virtual void Die()
    {
        m_dead = true;
    }

    public virtual void Jump()
    {
        //m_rigidBody.AddForce(Vector3.up * m_JumpPower * 100);
        m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, m_JumpPower, m_rigidBody.velocity.z);
    }

    #endregion

#region inhetiable non-overridable
    /// <summary>
    /// Rotates transform to face direction of movement.
    /// </summary>
    protected void ApplyTurnAround()
    {
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_TurnSpeed, m_forwardAmount);
        transform.Rotate(0, m_turnAmount * turnSpeed * Time.deltaTime, 0);
    }

    /// <summary>
    /// Function to improve gravitational pull on player for jumping.
    /// </summary>
    protected virtual void ApplyGravity()
    {

        Vector3 extraGravityForce = Vector3.up * Physics.gravity.y * (m_GravityMultiplier - 1) * Time.deltaTime;
        if (m_rigidBody.velocity.y < 0f)
        {
            m_rigidBody.AddForce(extraGravityForce);
        }
        //to keep unsure that the players rotation doesn't  fuck up.
        m_GroundedDistance = m_rigidBody.velocity.y < 0 ? m_originalGroundedDistance : m_GroundedDistance;

    }

    /// <summary>
    /// Checks if the player lands on any object via raycasting. 
    /// </summary>
    protected void CheckGrounded()
    {
        RaycastHit hitInfo;
        // Please do not touch any of the values right now.
        if (Physics.SphereCast(transform.position + Vector3.up * 0.1f, 0.1f, Vector3.down, out hitInfo, m_GroundedDistance))
        {
            SetGrounded(hitInfo.normal, true, true, hitInfo.transform);
            hitdistance = hitInfo.distance;
        }
        else
        {
            SetGrounded(Vector3.up, false, false, null);
            hitdistance = m_GroundedDistance;
        }

        //if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, m_GroundedDistance))
        //{
        //    m_groundNormal = hitInfo.normal;
        //    m_grounded = true;
        //    m_animator.applyRootMotion = true;
        //}
        //else
        //{
        //    m_grounded = false;
        //    m_groundNormal = Vector3.up;
        //    m_animator.applyRootMotion = false;
        //}

    }

#endregion

    /// <summary>
    /// Helps Visualize the raycast/boxcast
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Debug.DrawLine((transform.position + Vector3.up * 0.1f), (transform.position + Vector3.down * hitdistance));
        Gizmos.DrawWireSphere((transform.position + Vector3.up * 0.1f) + (Vector3.down * hitdistance), 0.1f);
    }

}
