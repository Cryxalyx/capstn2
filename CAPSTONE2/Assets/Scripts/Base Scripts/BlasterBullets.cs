﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterBullet : BulletInstancer {

    public Unit Parent;
    public float FireRate;
    public float ShotForce;
    public float LifeTime;
    public float RechargeTime;
    public float Cost;
    public int IconIndex;

    protected float m_lifeTimer;

    public virtual void Fire(Vector3 force)
    {
        GetComponent<Rigidbody>().AddForce(force);
    }

    protected bool Disperse()
    {
        m_lifeTimer += Time.deltaTime;
        return (m_lifeTimer > LifeTime);
    }
}
