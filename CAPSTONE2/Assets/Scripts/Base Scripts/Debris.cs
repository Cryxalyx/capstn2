﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour {

    public GameObject feedback;
    public LayerMask mask;
    public float m_Damage;
    GameObject spawnedFeedback;
    
	// Use this for initialization
	protected void Start () {
        SpawnVisualFeedback();
        if (m_Damage == 0)
            m_Damage = 10;
	}
	
    void SpawnVisualFeedback()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position,Vector3.down, out hit, Mathf.Infinity, mask))
        {
            spawnedFeedback = Instantiate(feedback, hit.point, Quaternion.identity);
            spawnedFeedback.GetComponentInChildren<GroundVisual>().SetOrigin(this.gameObject);
        }
    }

    void KeepPositionOnHit()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
        {
            spawnedFeedback.GetComponentInChildren<GroundVisual>().SetPosition(hit.point);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent(typeof(IDamageable)) != null && collision.gameObject.GetComponent<PlayerUnit>() != null)
        {
            IDamageable dmg = collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
            dmg.TakeDamage(m_Damage);
        }
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
        //KeepPositionOnHit();
		
	}
}
