﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

using UnityEngine;

public class Skill : MonoBehaviour {

    [SerializeField] protected float m_charge;
    [SerializeField] protected float m_maxCharge;
    [SerializeField] protected float m_coolDown;

    protected bool m_ability;
    protected bool m_secondaryAbility;
    protected bool m_cooleddown;
    protected float m_skillCooldownTimer;

    public float GetCooldown
    {
        get { return m_skillCooldownTimer/m_coolDown; }
    }

    /// <summary>
    /// Function to be called in Skill Control Script to call for skill inputs.
    /// </summary>
    //public virtual void AbilityControls()
    //{

    //}

    /// <summary>
    /// Function to be used by scripts outside of this class to call the ability.
    /// </summary>
	public virtual void Ability()
    {
       
    }

    /// <summary>
    /// Function to be used by scripts outside of this class to call the secondary ability.
    /// </summary>
    public virtual void SecondaryAbility()
    {
        
    }

    /// <summary>
    /// Function to be called when skill is selected as the current skill used by player.
    /// </summary>
    public virtual void OnAbilitySelected()
    {
        UIUpdater.TriggerEvent("UpdateMaxCharge", m_maxCharge);
        UIUpdater.TriggerEvent("UpdateCharge", m_charge);
    }

    /// <summary>
    /// Function to be called when skill is deselected by player.
    /// </summary>
    public virtual void OnAbilityDeselected()
    {

    }

    public virtual bool useAbility()
    {
        return m_ability;
    }

    public bool Cooldown()
    {
        m_skillCooldownTimer -= Time.deltaTime;
        if (m_skillCooldownTimer <= 0)
        {
            m_skillCooldownTimer = 0;
            return true;
        }
        return false;
    }

    public virtual bool useSecondaryAbility()
    {
        return m_secondaryAbility;
    }

    public float GetCharge()
    {
        return m_charge;
    }

    public float GetMaxCharge()
    {
        return m_maxCharge;
    }

    void Update()
    {

    }
}
