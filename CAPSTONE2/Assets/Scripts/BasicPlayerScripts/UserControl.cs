﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterControl))]
public class UserControl : MonoBehaviour {

    Transform m_Cam;
    Vector3 m_CamForward;
    CharacterControl m_CharControl;
    Vector3 m_Move;
    bool m_turnAround;
    bool m_Jump;

    // Use this for initialization
    void Awake () {
        if (Camera.main != null)
            m_Cam = Camera.main.transform;

        m_CharControl = GetComponent<CharacterControl>();
        m_turnAround = true;
	}

    void Start()
    {

    }

    void Update()
    {
        if(m_CharControl.IsGrounded() && !m_Jump)
            m_Jump = Input.GetButtonDown("Jump"); //Jump key is suppose to be space.
    }

    public void SetTurnCondition(bool turn)
    {
        m_turnAround = turn;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        m_turnAround = !Input.GetButton("Ability");
        float h = Input.GetAxis("Horizontal"); //Horizontal is a default Input in Project settings.
        float v = Input.GetAxis("Vertical");//Vertical is a default Input in Project settings.
        bool crouch = Input.GetKey(KeyCode.C);// Not yet implemented and still debatable. 

        if (m_Cam != null)
        {
            
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
            m_Move = v * Vector3.forward + h * Vector3.right;

        
            m_CharControl.Move(m_Move, crouch, m_Jump);


        //use this if we will use the camera's perspective to dictate the players direction and rotate as he moves.
        if (m_turnAround)
            m_CharControl.RotateCharacter(m_Move);

        //use this if we will use the camera's perspective to dictate where the player faces as the camera moves.
        if (!m_turnAround)
            m_CharControl.RotateCharacter(m_CamForward);
        m_Jump = false;

    }

}
