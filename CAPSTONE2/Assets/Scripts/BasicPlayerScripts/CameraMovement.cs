﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject targetToFollow;
    public GameObject PointofFocus;
    Vector3 cameraPos;
    Quaternion rotation;
    Vector3 currentPosition;
    float currentX;
    float currentY;
    float Y_direction;
    float X_direction;
    float OriginalDistance;
    public bool InvertX;
    public bool InvertY;
    public float yMin;
    public float yMax;
    public float m_ControllerSensitivity = 50f;
    public float m_MouseSensitivity = 100f;

    public float GetCameraY
    {
        get { return currentY; }
    }

    // Use this for initialization
    void Start ()
    {
        if (m_ControllerSensitivity == 0)
            m_ControllerSensitivity = 50f;
        if (m_MouseSensitivity == 0)
            m_MouseSensitivity = 100f;
        if (PointofFocus == null)
            PointofFocus = targetToFollow;
        cameraPos = transform.position - targetToFollow.transform.position;
        OriginalDistance = cameraPos.magnitude;

        InvertX = SingletonManager.Get<GameSettings>().InvertX;
        InvertY = SingletonManager.Get<GameSettings>().InvertY;
    }
	
    public Quaternion Rotation
    {
        get { return rotation; }
    }

    void CameraDirection()
    {
        if (InvertX)
        {
            X_direction = -1;
        }
        else
        {
            X_direction = 1;
        }

        if (InvertY)
        {
            Y_direction = -1;
        }
        else
        {
            Y_direction = 1;
        }
    }

    void RevolveCamera()
    {

        currentX += ((Input.GetAxisRaw("CameraAngleX") * m_ControllerSensitivity) + (Input.GetAxisRaw("Mouse X") * m_MouseSensitivity)) * Time.fixedDeltaTime * X_direction;
        currentY += ((Input.GetAxisRaw("CameraAngleY") * m_ControllerSensitivity) + (Input.GetAxisRaw("Mouse Y") * m_MouseSensitivity)) * Time.fixedDeltaTime * Y_direction;
        currentY = Mathf.Clamp(currentY, yMin, yMax);
        rotation = Quaternion.Euler(currentY, currentX, 0);
        currentPosition = targetToFollow.transform.position + rotation * cameraPos;
        transform.position = currentPosition;
        transform.LookAt(PointofFocus.transform.position);
    }

    public void SetPosition(Vector3 camPos)
    {
        cameraPos = camPos * OriginalDistance;
    }

	// Update is called once per frame
	void Update ()
    {
        CameraDirection();
        RevolveCamera();
	}
}
