﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillControl : MonoBehaviour {

    public List<Skill> m_abilities;
    public Skill m_currentSkill;
    int m_skillIndex;

    // Use this for initialization
    void Start () {
        m_abilities.AddRange(GetComponents<Skill>());
        m_skillIndex = 0;

        if (m_abilities.Count != 0)
            m_currentSkill = m_abilities[m_skillIndex];
        Setup();
    }
	
    void Setup()
    {
        if (m_currentSkill == null)
        {
            UIUpdater.TriggerEvent("MaxChargeSetUp", 0);
            UIUpdater.TriggerEvent("ChargeSetUp", 0);
            return;
        }
        UIUpdater.TriggerEvent("UpdateCoolDown", m_currentSkill.GetCooldown);
        UIUpdater.TriggerEvent("MaxChargeSetUp", m_currentSkill.GetMaxCharge());
        UIUpdater.TriggerEvent("ChargeSetUp", m_currentSkill.GetCharge());
    }

	// Update is called once per frame
	void Update () {
        UseSkills();
        ToggleSkills();
    }

    void ToggleSkills()
    {
        if (Input.GetButton("ToggleSkill") && m_skillIndex < m_abilities.Count - 1)
        {
            m_skillIndex++;
            SwitchSkill();
        }

        if (Input.GetButton("ToggleSkill") && m_skillIndex > 0)
        {
            m_skillIndex--;
            SwitchSkill();
        }
    }

    void UseSkills()
    {
        
        if (m_currentSkill == null)
            return;
        UIUpdater.TriggerEvent("UpdateCoolDown", m_currentSkill.GetCooldown); 
        m_currentSkill.Ability(); //Ability key is suppose to be mouse 0
        m_currentSkill.SecondaryAbility();//Ability2 key is suppose to be mouse 1
    }

    public Skill getSkill()
    {
        return m_currentSkill;
    }

    public void AddSkill(Skill skill) 
    {
        m_abilities.Add(skill);
        if (m_currentSkill != null)
            return;          
        UIUpdater.TriggerEvent("ChargeSetUp", m_currentSkill.GetCharge());
        UIUpdater.TriggerEvent("MaxChargeSetUp", m_currentSkill.GetMaxCharge());
    }

    /// <summary>
    /// Meant to handle skills on when to disable the gameobject of the skills and when to enable them.
    /// </summary>
    void SwitchSkill()
    {
        m_currentSkill.OnAbilityDeselected();
        m_currentSkill = m_abilities[m_skillIndex];
        m_currentSkill.OnAbilitySelected();
    }
}
