﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterControl : MonoBehaviour {

    public Transform m_SpawnPoint;
    public Transform m_AbsorbPoint;
    public List<GameObject> m_gadgets;
    public GameObject m_currentGadget;
    public float m_RechargeAmount;
    public GameObject AbsorbParSys;
    public AudioSource BlasterFire;
    [Range(1, 10)]
    public float m_ShotForceMultiplier;
    public float AimAngularOffset;
    [SerializeField] float m_charge;
    public bool m_AimmableBlaster;

    GadgetData currentGadgetData;
    Vector3 m_raycastDirection;
    Vector3 m_raycastCameraOffset;
    bool m_aimed;
    bool m_skillToggled;
    bool m_absorbCooldown;
    float m_maxCharge = 100;
    float m_fireTimer;
    float m_absorbCooldownTimer;
    float m_blasterIdleTime;
    float m_cameraY;
    float m_offSet;
    int m_gadgetIndex;


    // Use this for initialization
    void Start()
    {
        m_raycastDirection = transform.forward;
        m_gadgetIndex = 0;
        m_charge = m_maxCharge;
        Setup();
        if (m_gadgets.Count == 0)
            return;

    }

    public void UiSetUp()
    {
        UIUpdater.TriggerEvent("MaxChargeSetUp", m_maxCharge);
        UIUpdater.TriggerEvent("ChargeSetUp", m_charge);
        foreach (GameObject Gadget in m_gadgets)
        {
            SingletonManager.Get<PauseScreenUIController>().AddGadget(Gadget.GetComponent<GadgetDatahHolder>().Data);
        }
        if(m_gadgets.Count != 0)
        {
            currentGadgetData = m_gadgets[0].GetComponent<GadgetDatahHolder>().Data;
            SingletonManager.Get<PlayerGadgetUIController>().UpdateUI(currentGadgetData.Icon, currentGadgetData.Name);
        }
            
    }

    void Setup()
    {    
        if (m_gadgets.Count == 0 || m_currentGadget != null)
            return;
        m_currentGadget = m_gadgets[m_gadgetIndex];
        m_fireTimer = m_currentGadget.GetComponent<BlasterBullet>().FireRate;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_AimmableBlaster)
            RedirectAim();
        else
            m_raycastDirection = transform.forward;
        if (Input.GetAxis("Ability") > 0)
            Use();
        if (Input.GetAxis("Ability") < 0)
            Absorb();
        else
            AbsorbParSys.SetActive(false);

        if (m_aimed && Input.GetAxis("Ability") == 0 && m_currentGadget != null)
        {
            m_aimed = false;
            m_fireTimer = m_currentGadget.GetComponent<BlasterBullet>().FireRate;
        }
            

        if (Recharge() && m_charge < m_maxCharge)
        {
            m_charge += Time.deltaTime * m_RechargeAmount;
            UIUpdater.TriggerEvent("UpdateCharge", m_charge);
        }
        ToggleSkills();
    }

    int ToggleAxisToInt(float axis)
    {
        if (axis < 0)
            return -1;
        if (axis > 0)
            return 1;
        return 0;
    }

    void ToggleSkills()
    {
        if (!m_skillToggled && m_gadgets.Count != 0 && Input.GetAxisRaw("ToggleSkill") != 0)
        {
            m_gadgetIndex = (m_gadgetIndex + ToggleAxisToInt(Input.GetAxisRaw("ToggleSkill"))) % m_gadgets.Count;
            if (m_gadgetIndex < 0)
                m_gadgetIndex = m_gadgets.Count - 1;
            SwitchGadget();
            m_skillToggled = true;
        }
        if (m_skillToggled && Input.GetAxis("ToggleSkill") == 0)
            m_skillToggled = false;
    }

    void RedirectAim()
    {
        m_offSet = Camera.main.transform.parent.parent.GetComponent<CameraMovement>().GetCameraY;
        if (Mathf.Abs((int)m_offSet) % AimAngularOffset == 0 && Mathf.Abs((int)m_offSet) != 0)
            m_raycastCameraOffset = Camera.main.transform.parent.parent.forward;
        if (m_offSet < AimAngularOffset && m_offSet > -AimAngularOffset)
            m_raycastCameraOffset = Vector3.zero;

        m_raycastCameraOffset.x = 0;
        m_raycastCameraOffset.z = 0;
        m_raycastDirection = transform.forward + m_raycastCameraOffset;
        
    }

    void BlasterAimed()
    {
        m_aimed = true;
    }

    void Use()
    {
        if (m_currentGadget == null)
            return;

        if (!m_aimed)
            return;

        if (!Fire() || m_charge < m_currentGadget.GetComponent<BlasterBullet>().Cost)
            return;

        if(BlasterFire != null)
            BlasterFire.Play();
        m_blasterIdleTime = 0;
        GameObject instance = Instantiate(m_currentGadget, m_SpawnPoint.position, Quaternion.identity);
        instance.GetComponent<BlasterBullet>().Parent = GetComponent<Unit>();
        Vector3 direction = Vector3.Scale(transform.forward, Vector3.one).normalized * 1000 * m_ShotForceMultiplier;
        RaycastHit hit;
        if(Physics.Raycast(m_SpawnPoint.position, m_raycastDirection, out hit, Mathf.Infinity))
        {
            direction = hit.point - m_SpawnPoint.position;
            direction = direction.normalized * 1000 * m_ShotForceMultiplier;
        }

        instance.GetComponent<BlasterBullet>().Fire(direction);
        m_charge -= instance.GetComponent<BlasterBullet>().Cost;
        UIUpdater.TriggerEvent("UseCharge", instance.GetComponent<BlasterBullet>().Cost);
        m_fireTimer = 0;
    }

    void Absorb()
    {

        if (!m_aimed)
            return;

        AbsorbParSys.SetActive(true);
        RaycastHit hit;
        m_blasterIdleTime = 0;
        Vector3 castPoint = transform.position;
        castPoint.y = castPoint.y + (transform.localScale.y/2);
        if (Physics.SphereCast(castPoint, 1, transform.forward, out hit, 20) && IsAbsorbable(hit.collider))
        {
            Vector3 direction = (transform.position) - hit.collider.transform.position;
            direction.y = 0;
            hit.collider.GetComponent<Rigidbody>().AddForce(direction.normalized * 1000);
        }

        Collider[] cols = Physics.OverlapSphere(m_AbsorbPoint.position, 0.1f);
        foreach (Collider col in cols)
        {
            if (!IsAbsorbable(col))
                return;
            if (IsBulletInstancer(col.gameObject))
            {
                AddGadget(col.gameObject.GetComponent<BulletInstancer>().GetReference());
                Destroy(col.gameObject);
            }
            if (IsConsumable(col))
            {
                col.GetComponent<Consumables>().ApplyEffect(GetComponent<PlayerUnit>());
            }
            
        }

    }

    bool Fire()
    {
        if (m_currentGadget == null)
            return false;
        m_fireTimer += Time.deltaTime;
        return (m_fireTimer > m_currentGadget.GetComponent<BlasterBullet>().FireRate);
    }

    bool IsAbsorbable(Collider col)
    {
        return (col.gameObject.GetComponent(typeof(IAbsorbable)) != null);
    }

    bool IsBulletInstancer(GameObject absorbed)
    {
        return (absorbed.GetComponent<BulletInstancer>() != null);
    }

    bool IsConsumable(Collider col)
    {
        return (col.GetComponent<Consumables>() != null);
    }

    public void AddGadget(GameObject gadget)
    {
        if (m_currentGadget == null)
        {
            m_currentGadget = gadget;
            currentGadgetData = m_currentGadget.GetComponent<GadgetDatahHolder>().Data;
            m_fireTimer = m_currentGadget.GetComponent<BlasterBullet>().FireRate;
            SingletonManager.Get<PlayerGadgetUIController>().UpdateUI(currentGadgetData.Icon, currentGadgetData.Name);
        }
        foreach(GameObject gajet in m_gadgets)
        {
            if (gajet == gadget)
                return;
        }
        m_gadgets.Add(gadget);
        SingletonManager.Get<PauseScreenUIController>().AddGadget(gadget.GetComponent<GadgetDatahHolder>().Data);
    }

    /// <summary>
    /// Meant to handle skills on when to disable the gameobject of the skills and when to enable them.
    /// </summary>
    void SwitchGadget()
    {
        m_currentGadget = m_gadgets[m_gadgetIndex];
        currentGadgetData = m_currentGadget.GetComponent<GadgetDatahHolder>().Data;
        SingletonManager.Get<PlayerGadgetUIController>().UpdateUI(currentGadgetData.Icon, currentGadgetData.Name);
    }

    bool Recharge()
    {
        if (m_currentGadget == null && m_charge >= m_maxCharge)
            return false;
        m_blasterIdleTime += Time.deltaTime;
        return (m_blasterIdleTime > m_currentGadget.GetComponent<BlasterBullet>().RechargeTime);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(m_AbsorbPoint.position, 0.1f);
    }
}
