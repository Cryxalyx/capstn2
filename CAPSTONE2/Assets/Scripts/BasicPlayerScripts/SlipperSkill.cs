﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlipperSkill : Skill {

    public float SkillChargeRate;
    public GameObject slipper;
	// Use this for initialization
	void Start () {

    }

    public override void Ability()
    {
        if (Input.GetButton("Ability") && m_cooleddown)
            ChargeUp();
        if (Input.GetButtonUp("Ability") && m_cooleddown)
            GetComponent<Animator>().SetBool("Skill", true);
    }


    void ChargeUp()
    {
        
        GetComponent<UserControl>().SetTurnCondition(false);
        m_charge += SkillChargeRate;
        UIUpdater.TriggerEvent("UpdateCharge", m_charge); 
        m_charge = Mathf.Clamp(m_charge, 0, m_maxCharge);
    }

    void Launch()
    {
        m_cooleddown = false;
        GetComponent<UserControl>().SetTurnCondition(true);
        Vector3 spawnPoint = transform.position + transform.forward;
        spawnPoint.y = spawnPoint.y + 0.3f;
        Vector3 direction = Vector3.Scale(Camera.main.transform.forward, Vector3.one).normalized * m_charge * 10;

        GameObject m_slipper;
        m_slipper = Instantiate(slipper, spawnPoint, Quaternion.identity);
        m_slipper.GetComponent<Slipper>().Throw(direction);
        m_charge = 0;
        GetComponent<Animator>().SetBool("Skill", false);
        UIUpdater.TriggerEvent("UpdateCharge", m_charge);
        m_skillCooldownTimer = m_coolDown;
    }

    

    // Update is called once per frame
    void Update () {
        if (!m_cooleddown)
            m_cooleddown = Cooldown();

	}
}
