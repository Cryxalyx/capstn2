﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attached this script on a empty game object that is a child of the main player object. Make sure that it is set to be in front of the character based on the camera direction.
/// </summary>

[RequireComponent(typeof(Collider))]
public class Pillow : Gadget {

    PillowSkill pillowSkill;

    void Start()
    {
        pillowSkill = GetComponentInParent<PillowSkill>();
    }

    private void Update()
    {
        Debug.Log(pillowSkill.useAbility());
        Debug.Log(pillowSkill.useSecondaryAbility());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (pillowSkill.useAbility() && !pillowSkill.isFull())
        {
            //Absorbs the magnitude of the velocity of the object you blocked.
            pillowSkill.Absorb(other.attachedRigidbody.velocity.magnitude);
            other.attachedRigidbody.velocity = Vector3.zero;
        }

    }

    void OnTriggerStay(Collider other)
    {
        
        if (other.attachedRigidbody == null)
            return;


        if (pillowSkill.useSecondaryAbility())
        {
            //Launches the Object based on the force you absorbed.
            other.attachedRigidbody.AddForce(GetComponentInParent<Transform>().transform.forward * (pillowSkill.GetCharge() * Mathf.Pow(pillowSkill.getMultiplier(), 3)));
            pillowSkill.Deflect();
        }
    }

}
