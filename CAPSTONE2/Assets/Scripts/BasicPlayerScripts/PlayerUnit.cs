﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterControl))]
public class PlayerUnit : DamagingUnit, IDamageable {

    
    
    CharacterControl m_characterControl;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        m_characterControl = GetComponent<CharacterControl>();
    }

    public void SetUpHealth()
    {
        UIUpdater.TriggerEvent("HealthSetUp", m_baseHealth);
        UIUpdater.TriggerEvent("MaxHealthSetup", m_maxHealth);
    }

    public override void TakeDamage(float damage)
    {
        if (m_invulnerable)
            return;
        float damagePercentage = m_maxHealth / damage;
        m_characterControl.TakenDamage(damagePercentage);
        UIUpdater.TriggerEvent("TakeDamage", damage);
        base.TakeDamage(damage);
        m_invulnerable = true;         
    }



    public override bool Recovered()
    {
        return base.Recovered();
    }

    // Update is called once per frame
    void Update () {
        if(m_invulnerable && m_characterControl.HasRecovered())
            m_invulnerable = !Recovered();
        if (m_baseHealth <= 0)
            m_characterControl.Die();
	}
}
