﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillowSkill : Skill {

    [Range(1, 20)]
    [SerializeField]
    int m_multiplier;

    Vector3 parentPos;
    Vector3 cameraPos;
    bool m_absorptionFull;
    float subY;

    // Use this for initialization
    void Start()
    {
        if (m_maxCharge == 0)
            m_maxCharge = 5;
        
        m_absorptionFull = false;
        parentPos = GetComponentInParent<Transform>().transform.position;
    }

    /// <summary>
    /// Trial for making pillow look at the camera direction. will transfer to skill once it works.
    /// </summary>
    void VerticalRotation()
    {
        cameraPos = transform.position;
        subY += Input.GetAxis("CameraAngleY");
        float currentY = Mathf.Clamp(subY, 0, 50);
        Quaternion rotation = Quaternion.Euler(currentY, 0, 0);
        transform.position = parentPos + rotation * cameraPos;

    }

    public override void Ability()
    {
        base.Ability();
        //Checks on the status of the Gauge.
        if (m_charge >= m_maxCharge)
        {  
            m_charge = m_maxCharge;
            m_absorptionFull = true;
        }

    }

    public override void SecondaryAbility()
    {
       if (m_charge <= 0)
        {
            base.SecondaryAbility();
            return;
        }
    }

    void Update()
    {
        //VerticalRotation();
        if (m_charge < m_maxCharge)
            m_absorptionFull = false;
    }

    public void Absorb(float charge)
    {
        m_charge += charge;
    }

    public void Deflect()
    {
        m_charge = 0;
    }

    public bool isFull()
    {
        return m_absorptionFull;
    }

    public int getMultiplier()
    {
        return m_multiplier;
    }
}
