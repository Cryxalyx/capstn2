﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScope : MonoBehaviour
{

    public GameObject target;
    public List<GameObject> fadedObjs;
    public LayerMask layerMask;

    // Update is called once per frame
    void Update()
    {
        Scope();

    }


    void Scope()
    {
        RaycastHit[] hits;
        float dist = Vector3.Distance(this.transform.position, target.transform.position);
        hits = Physics.SphereCastAll(this.transform.position, 0.2f, this.transform.forward, dist - 1.0f, layerMask, QueryTriggerInteraction.UseGlobal);

        if (hits == null)
            return;

        List<GameObject> objs = new List<GameObject>();

        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];
            if (!(fadedObjs.Contains(hit.collider.gameObject)))
                fadedObjs.Add(hit.collider.gameObject);

            objs.Add(hit.collider.gameObject);

            Renderer rend = hit.transform.GetComponent<Renderer>();

            if (rend)
            {
                ToFade(rend);
            }
        }
        VisibleObjects(objs);
    }


    void ToFade(Renderer rend)
    {
        rend.material.SetFloat("_Mode", 2f);
        Color tmpColor = rend.material.color;
        tmpColor.a = 0.4f;
        rend.material.color = tmpColor;
        rend.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        rend.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        rend.material.SetInt("_ZWrite", 0);
        rend.material.DisableKeyword("_ALPHATEST_ON");
        rend.material.EnableKeyword("_ALPHABLEND_ON");
        rend.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        rend.material.renderQueue = 3000;
    }

    void VisibleObjects(List<GameObject> objs)
    {
        GameObject tmpObj;

        foreach (GameObject fadedObj in fadedObjs)
        {
            if (objs == null || !(objs.Contains(fadedObj)))
            {
                tmpObj = fadedObj;
                ToOpaque(fadedObj);
                fadedObjs.Remove(fadedObj);
                break;
            }
        }
    }

    void ToOpaque(GameObject obj)
    {
        Renderer rend = obj.GetComponent<Renderer>();
        if (rend)
        {


            rend.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            rend.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            rend.material.SetInt("_ZWrite", 1);
            rend.material.DisableKeyword("_ALPHATEST_ON");
            rend.material.DisableKeyword("_ALPHABLEND_ON");
            rend.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            rend.material.renderQueue = -1;
            rend.material.SetFloat("_Mode", 0f);
            Color tmpColor = rend.material.color;
            tmpColor.a = 1.0f;
            rend.material.color = tmpColor;
        }
    }
}