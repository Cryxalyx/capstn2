﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWallClip : MonoBehaviour {

    public float m_MinDistance = 0.1f;
    public float m_MaxDistance = 4.0f;
    public float m_Smooth = 10.0f;
    public float m_Distance;
    public Vector3 m_DollyDirAdjusted;

    Vector3 m_dollyDir;
    
	// Use this for initialization
	void Awake () {
        m_dollyDir = transform.localPosition.normalized;
        m_Distance = transform.localPosition.magnitude;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 desiredCameraPos = transform.parent.TransformPoint(m_dollyDir * m_MaxDistance);
        RaycastHit hit;

        if (Physics.Linecast(transform.parent.position, desiredCameraPos, out hit))
            m_Distance = Mathf.Clamp((hit.distance * 0.9f), m_MinDistance, m_MaxDistance);
        else
        {
            m_Distance = m_MaxDistance;
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, m_dollyDir * m_Distance, Time.deltaTime * m_Smooth);
	}
}
