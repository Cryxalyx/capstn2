﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book : Gadget, IDamageable {

    BookSkill bookSkill;
	// Use this for initialization
	void Start () {
        bookSkill = GetComponentInParent<BookSkill>();
	}

    // Update is called once per frame
    void Update () {
		
	}

    public void TakeDamage(float damage)
    {
        bookSkill.Shield(damage);
    }
}
