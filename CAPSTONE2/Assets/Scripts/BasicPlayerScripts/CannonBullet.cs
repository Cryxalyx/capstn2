﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : Projectiles {

	[SerializeField] Vector2 damageRange; // x min dmg and y max dmg
	int m_dmg; //Randomized damage depending on the damageRange ( min, max)

	// Use this for initialization
	void Start () {
		m_dmg = (int)Random.Range (damageRange.x, damageRange.y);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnCollisionEnter(Collision other) 
	{
		Debug.Log ("collide");
		this.gameObject.GetComponent<SphereCollider> ().enabled = false;
		this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		this.gameObject.SetActive (false);
	}
}
