﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamController : CharacterControl {

    
    public AudioSource FootSteps;

    [Range(10, 40)]
    public int m_KnockDownHpPercentage;
    public float m_JumpReleaseMultiplier;
    bool m_immovable;

    // Use this for initialization
    void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
        m_rigidBody.velocity = Vector3.zero;
        m_grounded = true;
        m_animator = GetComponent<Animator>();
        if (m_GroundedDistance == 0)
            m_GroundedDistance = (transform.localScale.y / 3);

        m_originalGroundedDistance = m_GroundedDistance;
        if (m_MovementMultiplier <= 0)
            m_MovementMultiplier = 0.1f;


    }

    void OnDisable(){
        m_rigidBody.velocity = Vector3.zero;
    }

    void OnAnimatorMove()
    {
        //Remove m_grounded in condition to allow movement while in air.
        Vector3 v = (m_move * m_MovementMultiplier) / Time.deltaTime;
            
        if (Time.deltaTime > 0 && !m_damaged && !m_dead)
        {
            //to Ensure that current velocity y isn't overwritten by new value.
            v.y = m_rigidBody.velocity.y;
            m_rigidBody.velocity = v * (Time.fixedDeltaTime * 50);
        }
    }

#region Movement and Rotation
    /// <summary>
    /// Used to Move the Character
    /// </summary>
    /// <param name="move"> is a computed vector3 velocity to be applied to the character.</param>
    /// <param name="crouch"> boolean to check if character is crouching or not</param>
    /// <param name="jump"> boolean to check if character is airborne or not</param>
    public override void Move(Vector3 move, bool crouch, bool jump)
    {


        //if (move.magnitude > 1f) move.Normalize();
        if (!m_immovable && !m_dead)
            m_move = move;
        else
            m_move = Vector3.zero;
        Vector3 tempMove = transform.InverseTransformDirection(move);
        CheckGrounded();
        move = Vector3.ProjectOnPlane(move, m_groundNormal);
        m_forwardAmount = tempMove.z;
        m_sideMotion = tempMove.x;

        if (!m_grounded)
        {
            ApplyGravity();
            //HandleAirMovement(move);
        }
        HandleMovement(jump);
        UpdateAnimator(move);

    }

    /// <summary>
    /// Handles players rotation based on a given Vector.
    /// </summary>
    /// <param name="move"> Vector three to which the model shall turn to</param>
    public override void RotateCharacter(Vector3 move)
    {
        Vector3 angularMovement = move;

        //move_ad = move;

        //Copmputation for changes the Transforms directions so that the model rotates towards direction.
        if (move.magnitude > 1f) move.Normalize();
        angularMovement = transform.InverseTransformDirection(angularMovement);
        angularMovement = Vector3.ProjectOnPlane(angularMovement, m_groundNormal);
        m_turnAmount = Mathf.Atan2(angularMovement.x, angularMovement.z);

        //m_forwardAmount = angularMovement.z;


        ApplyTurnAround();
    }
#endregion

#region overridables
    protected override void UpdateAnimator(Vector3 move)
    {
        m_animator.SetBool("Skill", Input.GetButton("Ability"));
        m_animator.SetFloat("Ability", Input.GetAxis("Ability"));
        m_animator.SetFloat("SideMotion", m_sideMotion, 0.1f, Time.deltaTime);
        m_animator.SetFloat("Forward", m_forwardAmount, 0.1f, Time.deltaTime);
        m_animator.SetInteger("Move", (int)m_rigidBody.velocity.magnitude);
        m_animator.SetBool("Grounded", m_grounded);
        m_animator.SetBool("Damaged", m_damaged);
        if (!m_grounded)
        {
            m_animator.SetFloat("YVelocity", m_rigidBody.velocity.y);
        }

        m_immovable = Immmovable();

        if (m_grounded && move.magnitude > 0)
        {
            m_animator.speed = 1;
        }
    }

    /// <summary>
    /// Function to hold any additional conditioning for changes in the movement of the character.
    /// </summary>
    /// <param name="jump"></param>
    protected override void HandleMovement(bool jump)
    {

        if (jump && m_animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
        {
            Jump();
            m_grounded = false;
            m_animator.applyRootMotion = false;

        }

    }

    public override void Die()
    {
        base.Die();
        GetComponent<UserControl>().enabled = false;
        Move(Vector3.zero, false, false);
        m_animator.SetBool("Dead", true);
    }

    void Dead()
    {
        Camera.main.transform.parent.transform.parent.GetComponent<CameraMovement>().enabled = false;
        m_rigidBody.isKinematic = true;
        LevelEventHandler.TriggerEvent("PlayerDead");
    }
    
    protected override void HandleAirMovement(Vector3 velocity)
    {
        m_move = velocity.normalized / 2;
    }

    protected override void ApplyGravity()
    {

        Vector3 extraGravityForce;
        if (m_rigidBody.velocity.y < 0f)
        {
            extraGravityForce = Vector3.up * Physics.gravity.y * (m_GravityMultiplier - 1) * Time.deltaTime;
            m_rigidBody.AddForce(extraGravityForce);
        }
        else if(m_rigidBody.velocity.y > 0f && !Input.GetButton("Jump"))
        {
            extraGravityForce = Vector3.up * Physics.gravity.y * ((m_GravityMultiplier * m_JumpReleaseMultiplier) - 1) * Time.deltaTime;
            m_rigidBody.AddForce(extraGravityForce);
        }
        //to keep unsure that the players rotation doesn't  fuck up.
        m_GroundedDistance = m_rigidBody.velocity.y < 0 ? m_originalGroundedDistance : m_GroundedDistance;

    }

    public override void Jump()
    {
        base.Jump();
    }

    public override void TakenDamage(float damagePercentage)
    {
        base.TakenDamage(damagePercentage);
        
        m_animator.SetTrigger("Damaged");
        m_animator.SetFloat("DamageAmount", (int)damagePercentage / m_KnockDownHpPercentage);
    }



    #endregion

    bool Immmovable()
    {
        return ((Input.GetButton("Ability") && Input.GetAxis("Ability") < 0) && m_grounded);
    }

    /// <summary>
    /// Helps Visualize the raycast/boxcast
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Debug.DrawLine((transform.position + Vector3.up * 0.1f), (transform.position + Vector3.down * hitdistance));
        Gizmos.DrawWireSphere((transform.position + Vector3.up * 0.1f) + (Vector3.down * hitdistance), 0.1f);
    }
}

