﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slipper : Gadget {

    bool m_throw;
	// Use this for initialization
	void Start () {
        
	}

    private void OnCollisionEnter(Collision collision)
    {
        BreakBreakable(collision);
        DamageDamageable(collision);
    }

    void BreakBreakable(Collision collision)
    {
        if (collision.gameObject.GetComponent(typeof(IBreakable)) as IBreakable == null)
            return;
        IBreakable breakable = collision.gameObject.GetComponent(typeof(IBreakable)) as IBreakable;
        breakable.Break();
    }

    void DamageDamageable(Collision collision)
    {
        if (collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable == null)
            return;
        IDamageable damageable = collision.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
        damageable.TakeDamage(5);
    }

    public void Throw(Vector3 force)
    {
        GetComponent<Rigidbody>().AddForce(force);
        m_throw = true;
    }

    // Update is called once per frame
    void Update () {
        if (m_throw && GetComponent<Rigidbody>().velocity.magnitude == 0)
        {
            Destroy(this.gameObject);
        }
            
	}
}
