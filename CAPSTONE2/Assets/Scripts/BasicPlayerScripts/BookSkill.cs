﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookSkill : Skill {

    public float m_ShieldRechargeTimer;
    public float m_rechargeValue;
    bool m_takenDamage;
    float m_timer;

	// Use this for initialization
	void Start () {
		m_charge = m_maxCharge;
	}
	
	// Update is called once per frame
	void  Update () {
		if(m_timer >= m_ShieldRechargeTimer)
        {
            m_takenDamage = false;
            m_timer = 0;
        }
        if (m_takenDamage)
        {
            m_timer += Time.deltaTime;
        }

        if(!m_takenDamage && m_charge < m_maxCharge)
        {
            m_charge += m_rechargeValue;
        }
	}

    public override void Ability()
    {
        if (m_charge < 0)
            return;
        base.Ability();
    }

    public override void SecondaryAbility()
    {
        base.SecondaryAbility();
    }

    public void Shield(float damage)
    {
        m_charge -= damage;
        m_takenDamage = true;
        m_timer = 0;
        
    }


}
