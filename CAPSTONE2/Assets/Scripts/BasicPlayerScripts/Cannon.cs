﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attached this script to a empty game object that is child of the main player. Make sure it is in front of the camera based on the camera direction.
/// </summary>
public class Cannon : Skill {
	[Range(1,10)]
	[SerializeField] int m_cannonSpeed; //the cannon's speed
	[SerializeField] GameObject m_object; //the cannon  || further upgrade, can be any object 
	Rigidbody m_objectRB; //rigidbody of the cannon
	bool isAlreadyLaunched; //is the cannon is already been launched ? 

	// Use this for initialization
	void Start () {
		
		if (m_cannonSpeed == 0)
			m_cannonSpeed = 20;
		m_objectRB = m_object.GetComponent<Rigidbody> ();
		m_objectRB.useGravity = false; // prevent the cannon from falling down
		isAlreadyLaunched = false;
	}

	public override void Ability ()
	{
		base.Ability ();
		if (m_ability) {
			m_object.GetComponent<SphereCollider> ().enabled = true;
			m_object.gameObject.SetActive(true);
			m_ability = false;
			//check if a cannon is already released
			if (isAlreadyLaunched) {
				m_object.transform.position = this.transform.position; //reposition the cannon in front of the player
				m_objectRB.velocity = this.transform.forward * (m_cannonSpeed * 5);
				return;
			}
			LaunchCannon ();
		}
	}

	public override void SecondaryAbility ()
	{

	}

	void LaunchCannon ()
	{
		m_object.transform.SetParent (this.GetComponentInParent<Transform> ().transform);
		m_object.transform.position = this.transform.position;
		m_objectRB.velocity = this.transform.forward * (m_cannonSpeed * 5);
		m_object.transform.SetParent (null);
		isAlreadyLaunched = true;
	}
		
	void Update() 
	{
		
	}
}
