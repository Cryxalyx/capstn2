﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameBlastDamager : MonoBehaviour {

    public float m_Distance;
    public float m_Radius;
    public float m_Damage;
    ParticleSystem ps;
	// Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();
	}
	
    void Fire()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, m_Radius, transform.right, out hit, m_Distance)){

            //limit = hit.point;
            if (hit.collider.gameObject.GetComponent(typeof(IDamageable)) != null)
            {
                IDamageable dmg = hit.collider.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
                dmg.TakeDamage(m_Damage * 2);
                Vector3 force = hit.point - transform.position;
                hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(force.normalized, ForceMode.Impulse);
            }
        }
    }

	// Update is called once per frame
	void Update () {
		if (ps.isPlaying && ps.time >= 2)
        {
            Fire();
        }
	}

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position + (transform.right * m_Distance), m_Radius);
    }
}
