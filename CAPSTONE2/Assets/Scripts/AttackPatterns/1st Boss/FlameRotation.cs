﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameRotation : AttackPattern {

    Direction direction;
    public List<ParticleSystem> m_ParSys;
    public List<FlameCollision> m_Flames;
    public float m_RotationTime;
    public float m_RotationSpeed;
    Transform m_transform;
    bool m_attack;
    float m_timer;
	// Use this for initialization
	void Start () {
        m_transform = GetComponent<Transform>();
        //m_Flames.AddRange(GetComponentsInChildren<FlameCollision>());
        //foreach(FlameCollision fc in m_Flames)
        //{
        //    m_ParSys.Add(fc.GetComponent<ParticleSystem>());
        //}
    }

    public override IEnumerator Attack()
    {
        int rand = Random.Range(0, 2);
        if (rand == 0)
            direction = Direction.Clockwise;
        else
            direction = Direction.CounterClockwise;
        foreach (FlameCollision fc in m_Flames)
        {
            fc.Damage = GetComponent<DamagingUnit>().Damage;
        }

        foreach (ParticleSystem ps in m_ParSys)
        {
            
            ps.Play();
        }
        yield return StartCoroutine(Rotate());
        m_isFinished = true;
    }

    /// <summary>
    /// Supposed to be for rotation but is not smoothed out.
    /// </summary>
    /// <returns></returns>
    IEnumerator Rotate()
    {
        while(true)
        {
            m_timer = m_timer + Time.deltaTime;
            rotate();
            if (m_timer > m_RotationTime)
            {
                m_timer = 0;
                break;
            }
            yield return null;
        }
        yield return null;
    }

    public override void StopAttack()
    {
        base.StopAttack();
        foreach(ParticleSystem ps in m_ParSys)
        {
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = false;
        }
    }

    void rotate()
    {
        m_transform.Rotate(Vector3.up,Time.deltaTime * m_RotationSpeed * (int)direction);
    }

    // Update is called once per frame
    void Update () {

    }
}
