﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameBlast : AttackPattern {

    public float ChargeUpTime;
    public GameObject eyes;
    // if single particle system.
    //public ParticleSystem ps;

    //if multiple.
    public List<ParticleSystem> m_ParSys;

    CharacterControl cc;
    BossBehavior bb;
    float m_chargeTimer;
    private void Start()
    {
        bb = GetComponent<BossBehavior>();
        cc = GetComponent<CharacterControl>();
        target = bb.target;
    }
    public override IEnumerator Attack()
    {
        yield return StartCoroutine(Aim());
        Fire();
        
    }

    IEnumerator Aim()
    {
        while(true)
        {
            
            Vector3 direction = target.transform.position - transform.position;
            direction.y = transform.position.y;
            cc.RotateCharacter(direction.normalized);
            if (ShouldFire())
                break;
            yield return null;
        }
        yield return null;
    }

    bool ShouldFire()
    {
        m_chargeTimer += Time.deltaTime;
        return (m_chargeTimer > ChargeUpTime);
    }
    void Fire()
    {
        //Insert particle system play here.

        /* If Single*/
        //ps.Play();


        /*if using List*/
        foreach (ParticleSystem ps in m_ParSys)
        {
            ps.GetComponent<FlameBlastDamager>().m_Damage = GetComponent<BossBehavior>().Damage;
            ps.Play();
        }
    }
    public override void EndAttack()
    {
        base.EndAttack();
        
        foreach(ParticleSystem ps in m_ParSys)
        {
            ps.time = 0;
        }
        m_chargeTimer = 0;
    }
    // Update is called once per frame
    void Update () {
        //Debug.Log(m_ParSys[0].time);
		if(m_ParSys[0].time >= 2 && !m_ParSys[0].isPlaying)
            m_isFinished = true;
        
    }
}
