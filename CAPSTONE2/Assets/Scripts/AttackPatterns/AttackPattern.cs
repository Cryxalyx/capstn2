﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackPattern : MonoBehaviour {

    public GameObject target;
    protected bool m_isFinished;

    public bool IsFinished
    {
        get { return m_isFinished; }
    }
    protected void ResetPattern()
    {
        m_isFinished = false;
    }

    public virtual void EndAttack()
    {
        ResetPattern();
    }

    // Use this for initialization
    void Start () {
		
	}

    public virtual void StopAttack()
    {
        StopAllCoroutines();
    }

    public abstract IEnumerator Attack();
}
