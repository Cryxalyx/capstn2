﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bombard : AttackPattern {

    public GameObject projectilePrefab;
    public List<GameObject> projectiles;
    public List<Vector3> placementPosition;
    public float Radius;
    public Transform m_SpawnPoint;
    public Vector3 spawnPoint;
    float m_radius;

    // Use this for initialization
    void Start()
    {
        target = GetComponent<DamagingUnit>().target;
        m_radius = Radius;
        if (m_SpawnPoint == null)
            m_SpawnPoint = transform;
    }


    public void SpawnProjectiles(List<Vector3> spawnPoints)
    {
        for (int i = 0; i < 6; i++)
        {
            projectiles.Add((GameObject)Instantiate(projectilePrefab, spawnPoints[i], Quaternion.identity));
        }
    }

    IEnumerator Setup()
    {
        for (float i = 0; i < 6; i++)
        {
            
            float posY = -Mathf.Cos(Mathf.PI * (i / 6));
            m_radius = (spawnPoint.normalized.magnitude * m_radius) - m_radius;
            m_radius = (spawnPoint.normalized.magnitude * m_radius) - Radius;


            float posX = spawnPoint.x + (m_radius * Mathf.Sin((2 * Mathf.PI) * (i / 6)));
            float posZ = spawnPoint.z + (m_radius * Mathf.Cos((2 * Mathf.PI) * (i / 6)));

            placementPosition.Add(new Vector3(posX, spawnPoint.y + 2, posZ));

        }
        SpawnProjectiles(placementPosition);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator ChargeUp()
    {
        foreach(GameObject go in projectiles)
        {
            go.AddComponent<RevolvingProjectiles>();
            go.GetComponent<RevolvingProjectiles>().parent = this.gameObject;
            yield return null;//new WaitForSeconds(0.5f);
        }
    }

    public override IEnumerator Attack()
    {
        m_isFinished = false;
        spawnPoint = m_SpawnPoint.position;
        yield return StartCoroutine(Setup());
        yield return StartCoroutine(ChargeUp());
        yield return new WaitForSeconds(5f);
        foreach (GameObject go in projectiles)
        {
            if (go == null)
                break;
            Destroy(go.GetComponent<RevolvingProjectiles>());
            go.AddComponent<SimpleProjectile>();
            go.GetComponent<SimpleProjectile>().speed = 50;
            go.GetComponent<SimpleProjectile>().SetTarget(target.transform.position);
            yield return new WaitForSeconds(0.5f);
        }

        m_isFinished = true;
        yield break;

    }

    public override void EndAttack()
    {
        base.EndAttack();
        projectiles.Clear();
        placementPosition.Clear();
    }

    public override void StopAttack()
    {
        base.StopAttack();
        foreach (GameObject proj in projectiles)
        {
            proj.GetComponent<OrbExplosive>().Explode();
        }
        
        projectiles.Clear();
        placementPosition.Clear();
        m_isFinished = true;
    }
}
