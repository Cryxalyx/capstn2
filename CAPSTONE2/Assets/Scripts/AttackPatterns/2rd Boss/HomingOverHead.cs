﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingOverHead : Homing {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 playerDis = target.transform.position;
        playerDis.y = this.transform.position.y;
        if (Vector3.Distance(playerDis, this.transform.position) < m_ClosestRange)
            m_closestRange = true;

        if (target == null)
            return;

        Vector3 playerDir = Direction();
        playerDir.y = 0;
        GetComponent<Rigidbody>().velocity = playerDir * speed;
    }

    protected override Vector3 Direction()
    {
        Vector3 playerDir = target.transform.position;
        playerDir.y = this.transform.position.y;
        if (Vector3.Distance(playerDir, this.transform.position) > m_ClosestRange && !m_closestRange)
        {

            direction = target.transform.position - this.transform.position;
        }


        return direction;
    }
}
