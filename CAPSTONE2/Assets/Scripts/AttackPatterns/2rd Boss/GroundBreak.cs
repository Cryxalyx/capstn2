﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundBreak : AttackPattern {

    //public float elevationSpeed;
    //public float timeDelay;
    //public float slamSpeed;
    public GameObject GroundVisual;
    GameObject visual;
    Vector3 direction;
    CharacterControl characterControl;
    bool m_launched;
    bool m_look;
    // Use this for initialization
    void Start () {
        characterControl = GetComponent<CharacterControl>();
        target = GetComponent<Boss>().target;
    }

    public override IEnumerator Attack()
    {
        m_look = true;
        m_isFinished = false;
        GetComponent<Animator>().SetFloat("AttackIndex", 2);
        GetComponent<Animator>().SetBool("Attack", true);
        direction = target.transform.position - transform.position;
        //Vector3 visualSpawn = target.transform.position;
        //visualSpawn.y = 0.1f;
        //visual = Instantiate(GroundVisual, visualSpawn, Quaternion.identity);
        //visual.GetComponent<GroundVisualPlain>().SetOrigin(this.gameObject);
        direction.y = 0;
        yield return StartCoroutine(LookAt());
        yield return null;
    }

    IEnumerator LookAt()
    {
        while(m_look)
        {

            characterControl.RotateCharacter(direction.normalized);
            if (!m_look)
                break;
            yield return null;
        }
        yield return null;
    }

    void SpawnGroundVisual()
    {

    }

    public void AddJumpForwardForce()
    {
        m_launched = true;      
    }

    void Land()
    {
        m_look = false;
        characterControl.Move(Vector3.zero, false, false);
        m_isFinished = true;
        LevelEventHandler.TriggerEvent("GroundBreak");
        
    }

    public override void EndAttack()
    {
        base.EndAttack();
        m_launched = false;
        GetComponent<Animator>().SetBool("Attack", false);
    }

    public override void StopAttack()
    {
        GetComponent<Animator>().SetBool("Attack", false);
        base.StopAttack();
        m_isFinished = true;
    }

    // Update is called once per frame
    void Update ()
    {       
        
        if(m_launched)
        {
            float dist = Vector3.Distance(transform.position, target.transform.position);
            Vector3 force = direction.normalized * dist;
            force.y = GetComponent<Rigidbody>().velocity.y;
            characterControl.Move(force, false, false);
        }
            
            
    }
}
