﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gatling : AttackPattern {

    public float m_AttackTime;
    public List<ParticleSystem> m_parSys;

    float m_attackTimer;

    private void Start()
    {
        target = GetComponent<DamagingUnit>().target;
    }

    public override IEnumerator Attack()
    {
        GetComponent<Animator>().SetFloat("AttackIndex", 1);
        GetComponent<Animator>().SetBool("Attack", true);
        yield return StartCoroutine(LookAt());
        yield return null;
    }

    
    public void FireGatling()
    {
        
        foreach (ParticleSystem ps in m_parSys)
        {
            ps.Play();
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = true;
        }
    }

    IEnumerator LookAt()
    {
        while (true)
        {
            m_parSys[1].transform.LookAt(target.transform.position);
            Vector3 direction = target.transform.position - transform.position;
            direction.y = transform.position.y;
            GetComponent<CharacterControl>().RotateCharacter(direction.normalized);
            if (End())
            {
                m_isFinished = true;
                break;
            }  
            yield return null;
        }
        yield return null;
    }

    bool End()
    {

        m_attackTimer += Time.deltaTime;
        return (m_attackTimer > m_AttackTime);
    }

    public override void EndAttack()
    {
        base.EndAttack();
        GetComponent<Animator>().SetBool("Attack", false);
        foreach (ParticleSystem ps in m_parSys)
        {
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = false;
            ps.Stop();
        }
        m_attackTimer = 0;

    }

    public override void StopAttack()
    {
        base.StopAttack();
        GetComponent<Animator>().SetBool("Attack", false);
        foreach (ParticleSystem ps in m_parSys)
        {
            ParticleSystem.EmissionModule em = ps.emission;
            em.enabled = false;
            ps.Stop();
        }
        m_attackTimer = 0;
        m_isFinished = true;
    }
}
