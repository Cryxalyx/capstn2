﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBlast : AttackPattern {

    public GameObject m_ProjectilePrefab;
    public Transform m_SpawnPoint;
    public float m_FiringForce;
    GameObject Bullet;
    Vector3 m_direction;
    bool m_look;
	// Use this for initialization
	void Start () {
        target = GetComponent<DamagingUnit>().target;
	}

    public override IEnumerator Attack()
    {
        m_isFinished = false;
        m_look = true;
        GetComponent<Animator>().SetFloat("AttackIndex", 0);
        GetComponent<Animator>().SetBool("Attack", true);
        m_direction = target.transform.position - transform.position;
        m_direction.y = 0;
        yield return StartCoroutine(LookAt());
        yield return null;
    }

    public IEnumerator LookAt()
    {
        while(m_look)
        {
            GetComponent<CharacterControl>().RotateCharacter(m_direction);
            if (!m_look)
                break;
            yield return null;
        }
        yield return null;
        
    }

    public override void EndAttack()
    {
        base.EndAttack();
        GetComponent<Animator>().SetBool("Attack", false);
        m_look = false;
    }

    void FireCannon()
    {
        if (Bullet == null)
            Bullet = Instantiate(m_ProjectilePrefab, m_SpawnPoint.position, Quaternion.identity);
        Bullet.transform.position = m_SpawnPoint.position;
        Bullet.GetComponent<Homing>().target = target;
        Bullet.GetComponent<Rigidbody>().AddForce(m_direction.normalized * m_FiringForce);
    }

    void Recoil()
    {
        m_isFinished = true;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
