﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject GameOverSelected;
    public GameObject GameoverObjects;
    public MenuUIController MenuUI;
    public bool DisableMouseCursor;
    bool pause;
	// Use this for initialization
	void Awake () {
        if (DisableMouseCursor)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            
        }
	}


	// Update is called once per frame
	void Update () {
        Pause();
        LevelEventHandler.StartListening("PlayerDead", GameOver);
    }

    public void Pause()
    {
        if (Input.GetButtonDown("Start"))
        {
            pause = !pause;
            if (pause)
                MenuUI.OpenMenu();
            else
                MenuUI.CloseMenu();
            
            Camera.main.transform.parent.parent.GetComponent<CameraMovement>().enabled = !pause;
            Time.timeScale = Convert.ToInt32(!pause);
            
        }
    }

    void GameOver()
    {
        GameoverObjects.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameOverSelected);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Restart()
    {
        GameInstance.LoadGameMode(GameInstance.GetGameMode().gameObject.scene.name);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
