﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBehavior : MonoBehaviour {

    public GameObject explosion;
    protected float timer;
    public float timeLimit;
    // Use this for initialization
    void Start () {

	}

    public void Explode()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        DestroyObject(this.gameObject);

    }

}
