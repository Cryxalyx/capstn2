﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehavior : Boss, IDamageable
{

    protected AttackPattern m_currentAttack;
    protected CharacterControl m_characterControl;
    int m_attackPatternIndex;
    float m_attackTransitionTime;
    float m_missingHealthPercentage;
    float m_attackIndexCounter;

    public BossUIData UIBossData;
    public List<AttackPattern> AttackPatterns;
    public GameObject DeathEffect;
    public Transform DeathEffectSpawnPoint;
    public float BaseAttackTransitionTime;
    public float MinAtkTransitionTime;
    public float AttackPatternSwitchRate;

    public AttackPattern CurrentAttackPattern
    {
        get { return m_currentAttack; }
        set { m_currentAttack = value; }
    }

    protected override void Start()
    {
        base.Start();
        target = GetComponent<BossBehavior>().target;
        m_characterControl = GetComponent<CharacterControl>();
        UnitSetup();
        
    }

    public void SetUpUI()
    {
        UIUpdater.TriggerEvent("BossHealthSetUp", Health);
    }

    public override void Die()
    {
        if (m_currentAttack != null)
            m_currentAttack.StopAttack();
        m_characterControl.Die();
    }


    /// <summary>
    /// Returns the Attack Index to be used by states.
    /// </summary>
    public int AttackIndex
    {
        get { return m_attackPatternIndex; }
        set { m_attackPatternIndex = value; }
    }

    /// <summary>
    /// Returns the time that needs to elapse before going to next attack.
    /// </summary>
    public float AttackTransitionTime
    {
        get { return m_attackTransitionTime; }
    }

    public void EndCurrentAttack()
    {
        if(m_currentAttack != null)
            m_currentAttack.StopAttack();
    }

    #region private functions
    // Use this for initialization

    /// <summary>
    /// Sets up Boss Attackpatterns called on start.
    /// </summary>
    void UnitSetup()
    {
        m_missingHealthPercentage = Health / MaxHealth;
        m_attackTransitionTime = BaseAttackTransitionTime;
        Damage = BaseDamage;
        int size = GetComponents<AttackPattern>().Length;
        for (int i = 0; i < size; i++)
        {
            AttackPatterns.Add(GetComponents<AttackPattern>()[i]);
        }
        foreach (AttackPattern ap in AttackPatterns)
        {
            ap.target = target;
        }
    }

    /// <summary>
    /// Update Attack Pattern Damages and Delays depending on Life left.
    /// </summary>
    void UpdateStats()
    {
        if (m_missingHealthPercentage == Health / MaxHealth)
            return;
        //Increases Damage and Shortens Transition time based on Health percentage.
        m_missingHealthPercentage = Health / MaxHealth;
        float AttackTransitionFactor = (BaseAttackTransitionTime * m_missingHealthPercentage);
        float DamageUpFactor = BaseDamage - (BaseDamage * m_missingHealthPercentage);
        Damage = BaseDamage + DamageUpFactor;
        m_attackTransitionTime = (AttackTransitionFactor) + MinAtkTransitionTime;
        
    }

    void UpdateAttackIndex()
    {
        m_attackIndexCounter = (m_attackIndexCounter + AttackPatternSwitchRate) % AttackPatterns.Count;
        m_attackPatternIndex = (int)m_attackIndexCounter % AttackPatterns.Count;
    }

    /// <summary>
    /// For Testing Purposes
    /// </summary>
    //void AttackPAtternActivation()
    //{

    //    for (int i = 0; i < 10; i++)
    //    {
    //        if (Input.GetKeyDown("" + i) && i < AttackPatterns.Count)
    //        {
    //            m_currentAttack = AttackPatterns[i];
    //        }
    //    }
    //}




    #endregion

    public override void TakeDamage(float damage)
    {
        if (m_invulnerable)
            return;
        m_characterControl.TakenDamage(damage);
        base.TakeDamage(damage);
        UIUpdater.TriggerEvent("BossTakeDamage", damage);
    }

    /// <summary>
    /// Function to be called by Attack State to start Coroutine.
    /// </summary>
    public void Attack()
    {
        m_currentAttack = AttackPatterns[m_attackPatternIndex];
        StartCoroutine(m_currentAttack.Attack());
        UpdateAttackIndex();
        UpdateStats();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_invulnerable)
            m_invulnerable = !Recovered();
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent(typeof(IDamageable)) != null)
        {
            IDamageable dmg = collision.collider.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
            dmg.TakeDamage(Damage * 2);
            Vector3 force = collision.collider.transform.position - transform.position;
            collision.collider.gameObject.GetComponent<Rigidbody>().AddForce(force.normalized, ForceMode.Impulse);
        }
    }
}
