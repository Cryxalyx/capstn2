﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmegaBullEUnit : BossBehavior {

    
    
    // Update is called once per frame
    void Update()
    {
        if (m_invulnerable)
            m_invulnerable = !Recovered();
    }

    public override void TakeDamage(float damage)
    {
        if (m_invulnerable)
            return;
        m_characterControl.TakenDamage(damage);
        base.TakeDamage(damage);
        m_invulnerable = true;
        UIUpdater.TriggerEvent("BossTakeDamage", damage);
    }



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent(typeof(IDamageable)) != null)
        {
            IDamageable dmg = collision.collider.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
            dmg.TakeDamage(Damage * 2);
            Vector3 force = collision.collider.transform.position - transform.position;
            collision.collider.gameObject.GetComponent<Rigidbody>().AddForce(force.normalized * 2, ForceMode.Impulse);
        }
    }
}
