﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenUnitController : CharacterControl {

    public float Damage;
    public override void Move(Vector3 move, bool crouch, bool jump)
    {
        base.Move(move, crouch, jump);
        //Remove m_grounded in condition to allow movement while in air.
        Vector3 v = (m_move * m_MovementMultiplier) / Time.deltaTime;
        if (m_grounded && Time.deltaTime > 0 && !m_damaged)
        {
            //to Ensure that current velocity y isn't overwritten by new value.
            v.y = m_rigidBody.velocity.y;
            m_rigidBody.velocity = v;
        }
    }

    /// <summary>
    /// Helps Visualize the raycast/boxcast
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Debug.DrawLine((transform.position + Vector3.up * 0.1f), (transform.position + Vector3.down * hitdistance));
        Gizmos.DrawWireSphere((transform.position + Vector3.up * 0.1f) + (Vector3.down * hitdistance), 0.1f);
    }
}
