﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryManager : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MissingMemoryTrigger>() == null)
            return;
        other.GetComponent<MissingMemoryTrigger>().TriggerMemory();
    }
}
