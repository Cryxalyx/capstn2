﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCannon : MonoBehaviour {


    public GameObject m_projectilePrefab;
    public float m_TimerLimit;
    public float m_FireForce;
    Vector3 m_spawnPoint;
    float m_timer;
	// Use this for initialization
	void Start () {
        m_spawnPoint = GetComponentInChildren<Transform>().localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        m_timer += Time.deltaTime;
		if(m_timer >= m_TimerLimit)
        {
            GameObject cannonBall = Instantiate(m_projectilePrefab, m_spawnPoint, Quaternion.identity);
            cannonBall.GetComponent<Rigidbody>().AddForce(transform.forward * m_FireForce);
            m_timer = 0;
        }
	}
}
