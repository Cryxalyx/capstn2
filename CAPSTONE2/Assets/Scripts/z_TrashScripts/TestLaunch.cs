﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLaunch : MonoBehaviour {


    [SerializeField]float m_storedForce;
    [Range(10,20)]
    public int multiplier;
	void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody)
        {
            if (Input.GetButtonDown("Ability"))
            {
                other.attachedRigidbody.AddForce(GetComponentInParent<Transform>().transform.forward * (m_storedForce * Mathf.Pow(multiplier, 3)));
                m_storedForce = 0;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Input.GetButton("Ability2"))
        {
            m_storedForce += other.attachedRigidbody.velocity.magnitude;
            other.attachedRigidbody.velocity = Vector3.zero;
        }
    }
}
