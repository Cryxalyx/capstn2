﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyShieldDisabler : MonoBehaviour {

    public List<ObjectBasedExistence> shields;
    ObjectBasedExistence currentShield;
    public float DisableTime;
    public float SelectionDelay;
    int m_obsIndex;
    float m_selectionTimer;
    float m_disableTimer;
	// Use this for initialization
	void Start () {
        shields.AddRange(GetComponentsInChildren<ObjectBasedExistence>());
	}
	
    bool DisableShield()
    {
        if(currentShield == null)
            m_selectionTimer += Time.deltaTime;
        return (m_selectionTimer > SelectionDelay);
    }

    bool EnableShield()
    {
        if (currentShield != null)
            m_disableTimer += Time.deltaTime;
        return (m_disableTimer > DisableTime);
    }

    ObjectBasedExistence NextShield()
    {
        m_obsIndex = (m_obsIndex + 1) % shields.Count;
        return shields[m_obsIndex];
    }


    void CheckForNull()
    {
        for(int i = 0; i < shields.Count; i++)
        {
            if(shields[i] == null)
            {
                shields.RemoveAt(i);
                m_obsIndex = Random.Range(0, shields.Count);
                break;
            }
        }
    }
	// Update is called once per frame
	void Update () {
        if (shields.Count <= 0)
            Destroy(gameObject);

        if (DisableShield())
        {
            currentShield = NextShield();
            m_selectionTimer = 0;
            currentShield.gameObject.SetActive(false);
        }
        if (EnableShield())
        {
            currentShield.gameObject.SetActive(true);
            currentShield = null;
            m_disableTimer = 0;
        }

        CheckForNull();
	}
}
