﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissingMemory : MonoBehaviour, IAbsorbable {

    public int m_MemoryNumber;
    public Material m_originalMaterial;
	// Use this for initialization
	void Start () {
        this.GetComponent<Renderer>().material = null;
    }

    void RecoverMemory()
    {
        Destroy(this);
    }

    private void OnDestroy()
    {
        GetComponent<Collider>().enabled = true;
        this.GetComponent<Renderer>().material = m_originalMaterial;
        LevelEventHandler.StopListening("Missing Memory " + m_MemoryNumber, RecoverMemory);
    }

    private void OnDisable()
    {
        LevelEventHandler.StopListening("Missing Memory " + m_MemoryNumber, RecoverMemory);
    }

    private void OnEnable()
    {
        LevelEventHandler.StartListening("Missing Memory " + m_MemoryNumber, RecoverMemory);
        GetComponent<Collider>().enabled = false;
        this.GetComponent<Renderer>().material = null;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
