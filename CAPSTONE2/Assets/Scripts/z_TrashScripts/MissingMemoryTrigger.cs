﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissingMemoryTrigger : MonoBehaviour {

    public int m_MemoryTriggerNumber;
    public void TriggerMemory()
    {
        LevelEventHandler.TriggerEvent("Missing Memory " + m_MemoryTriggerNumber);
    }
}
