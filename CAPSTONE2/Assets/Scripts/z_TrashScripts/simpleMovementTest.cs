﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simpleMovementTest : MonoBehaviour {


    float xMovement;
    float zMovement;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        xMovement = Input.GetAxis("Horizontal");
        zMovement = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(xMovement/2, 0, zMovement/2);
        transform.position += movement;
	}
}
