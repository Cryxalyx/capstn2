﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimTrigger : MonoBehaviour {

    public string EventName;
    private PlayerUnit playerUnit;
	// Use this for initialization
	void Start () {
        playerUnit = GameInstance.GetActiveGameMode<LevelScene>().Player;
	}

    private void OnDestroy()
    {
        playerUnit.GetComponent<SamController>().Move(Vector3.zero, false, false);
        playerUnit.GetComponent<UserControl>().enabled = false;
        LevelEventHandler.TriggerEvent(EventName);
    }

    //   // Update is called once per frame
    //   void Update () {
    //	if(playerUnit.GetComponent<BlasterControl>().m_gadgets.Count == 2)
    //       {
    //           playerUnit.GetComponent<SamController>().Move(Vector3.zero, false, false);
    //           playerUnit.GetComponent<UserControl>().enabled = false;
    //           LevelEventHandler.TriggerEvent(EventName);
    //           Destroy(this);
    //       }
    //}
}
