﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MiniBullEBattleEvent : MonoBehaviour {

    public List<BullEController> Bullies;
    float m_timer;
	// Use this for initialization
	void Start () {
		
	}
	
    bool AllDead()
    {
        foreach(BullEController bulle in Bullies)
        {
            if (!bulle.IsDead)
            {
                return false;
            }
        }
        return true;
    }

	// Update is called once per frame
	void Update () {
        
        if (AllDead())
        {
            m_timer += Time.deltaTime;
        }
        if(m_timer > 5)
        {
            GetComponent<NextLevelTrigger>().LoadNextScene();
            this.enabled = false;
        }
            
    }
}
