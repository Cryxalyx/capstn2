﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamager : MonoBehaviour {

    public float Damage;
    public float ExplosionRadius;

    private void OnDestroy()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, ExplosionRadius);

        foreach (Collider col in cols)
        {
            if (col.GetComponent<PlayerUnit>() != null)
            {
                IDamageable dmg = col.GetComponent(typeof(IDamageable)) as IDamageable;
                dmg.TakeDamage(Damage);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, ExplosionRadius);
    }
}
