﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IGoombaUnit : DamagingUnit {

    protected override void Start()
    {
        base.Start();
        m_dead = false;
    }
    private void OnCollisionEnter(Collision collision)
    {

        GameObject tmp = collision.collider.gameObject;

        if ((tmp.transform.position.y) >= (transform.position.y) && tmp.GetComponent<PlayerUnit>() != null)
        {
            Destroy(this.gameObject);
        }

        else if (tmp.GetComponent<PlayerUnit>() != null)
        {
            tmp.GetComponent<PlayerUnit>().TakeDamage(Damage);
            Vector3 force = collision.collider.transform.position - transform.position;
            force.y = 2;
            tmp.gameObject.GetComponent<Rigidbody>().AddForce(force.normalized * 5, ForceMode.Impulse);
        }
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
    }   

    void Update()
    {
        if (m_baseHealth <= 0 && !m_dead)
        {
            Die();
            m_dead = true;
        }
            

        if (m_dead)
            this.gameObject.SetActive(false);
    }
}
