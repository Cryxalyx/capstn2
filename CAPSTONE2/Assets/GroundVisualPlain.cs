﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundVisualPlain : MonoBehaviour {

    GameObject origin;

    private void Start()
    {
        LevelEventHandler.StartListening("GroundBreak", DisperseThis);
    }

    private void DisperseThis()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        LevelEventHandler.StopListening("GroundBreak", DisperseThis);
    }

    public void SetOrigin(GameObject Origin)
    {
        origin = Origin;
    }

    private void Update()
    {
        if (origin == null)
            Destroy(gameObject);
    }
}
