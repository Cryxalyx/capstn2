﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndGame : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
        Time.timeScale = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Jump") || Input.GetButton("Start"))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
            
	}
}
