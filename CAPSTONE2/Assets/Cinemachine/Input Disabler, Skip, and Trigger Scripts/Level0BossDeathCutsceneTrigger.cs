﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level0BossDeathCutsceneTrigger : MonoBehaviour {

    [SerializeField] private GameObject cutsceneTimeline;
    [SerializeField] private GameObject skipHandler;
    [SerializeField] private GameObject skipTimeline;
    [SerializeField] private GameObject cmBrain;

	// Use this for initialization
	void Start ()
    {
        LevelEventHandler.StartListening("BossDeath", EnableTimeline);
	}

    private void EnableTimeline()
    {
        cutsceneTimeline.SetActive(true);
        skipTimeline.SetActive(false);
        skipHandler.SendMessage("SetOriginalTimeline", cutsceneTimeline, SendMessageOptions.DontRequireReceiver);
        cmBrain.SetActive(true);
    }

    private void OnDisable()
    {
        LevelEventHandler.StopListening("BossDeath", EnableTimeline);

    }
}
