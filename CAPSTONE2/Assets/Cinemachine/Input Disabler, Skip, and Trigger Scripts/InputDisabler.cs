﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDisabler : MonoBehaviour
{
    /// <summary>
    /// this script will disable the input of the player for character control
    /// </summary>

    [SerializeField] private GameObject controller; //the object with the control script
    [SerializeField] private GameObject cameraController; //the object that holds the control of the camera
    
	void OnEnable () {

        if (controller && controller.GetComponent<UserControl>())
            controller.GetComponent<UserControl>().enabled = false;

        if (controller.GetComponent<CharacterControl>())
            controller.GetComponent<CharacterControl>().enabled = false;

        if (controller.GetComponent<BlasterControl>())
            controller.GetComponent<BlasterControl>().enabled = false;

        if (cameraController && cameraController.GetComponent<CameraMovement>())
            cameraController.GetComponent<CameraMovement>().enabled = false;
    }
	
	void OnDisable () {
        if (controller && controller.GetComponent<UserControl>())
            controller.GetComponent<UserControl>().enabled = true;

        if (controller.GetComponent<CharacterControl>())
            controller.GetComponent<CharacterControl>().enabled = true;


        if (controller.GetComponent<BlasterControl>())
            controller.GetComponent<BlasterControl>().enabled = true;

        if (cameraController && cameraController.GetComponent<CameraMovement>())
            cameraController.GetComponent<CameraMovement>().enabled = true;
    }
}
