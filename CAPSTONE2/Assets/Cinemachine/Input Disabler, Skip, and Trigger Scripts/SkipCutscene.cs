﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCutscene : MonoBehaviour
{
    [SerializeField] GameObject skipTimeline;
    [SerializeField] GameObject originalTimeline;
    [SerializeField] GameObject inputDisabler;
	[SerializeField] GameObject swapper;
     
	public void Skip ()
    {
        skipTimeline.SetActive(true);
        inputDisabler.SetActive(false);
        originalTimeline.SetActive(false);

		if(swapper)
			swapper.SetActive (false);
	}

    public void SetOriginalTimeline (GameObject timeline)
    {
        originalTimeline = timeline;
    }
	
	void Update ()
    {
        if (Input.GetButtonDown("Jump") || Input.GetButtonDown("Start"))
            Skip();
        if (!originalTimeline.activeSelf)
            this.gameObject.SetActive(false);
	}
}
