﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineBrainHandler : MonoBehaviour {

    [SerializeField] private GameObject cmBrain;

    private void OnEnable()
    {
        if (cmBrain)
            cmBrain.GetComponent<Cinemachine.CinemachineBrain>().enabled = true;
    }

    private void OnDisable()
    {
        if (cmBrain)
        {
            cmBrain.GetComponent<Cinemachine.CinemachineBrain>().enabled = false;
            //cmBrain.transform.position = cmBrain.transform.parent.transform.position;
            //cmBrain.transform.rotation = cmBrain.transform.parent.transform.rotation;
        }
    }
}
