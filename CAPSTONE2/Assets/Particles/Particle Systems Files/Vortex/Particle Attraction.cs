﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleAttraction : MonoBehaviour {

    public Transform targetPos;
    public float speed;

    private ParticleSystem mParticleSystem;

    private void Start()
    {
        if (!mParticleSystem)
            mParticleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update ()
    {
        //Check if the particle system is emitting
        if (mParticleSystem.isEmitting)
        {
            
            

        }
	}
}
