﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGadgetUIController : MonoBehaviour {

    public Image Icon;
    public Text Name;

    private void Awake()
    {
        SingletonManager.Register(this);
        HideUI();
    }

    public void ShowUI()
    {
        Icon.gameObject.SetActive(true);
        Name.gameObject.SetActive(true);
    }

    public void HideUI()
    {
        Icon.gameObject.SetActive(false);
        Name.gameObject.SetActive(false);
}

    public void UpdateUI(Sprite icon, string name)
    {
        Name.text = name;
        Icon.sprite = icon;
        ShowUI();
    }
}
