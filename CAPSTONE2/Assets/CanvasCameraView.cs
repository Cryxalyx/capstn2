﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCameraView : MonoBehaviour {
    Camera m_cam;

    // Use this for initialization
    void Start () {
        
        m_cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + m_cam.transform.rotation * Vector3.forward, m_cam.transform.rotation * Vector3.up);

    }
}
