﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndingUiController : MonoBehaviour {

    public string TitleScreen;
    public string EndingEventName;
    public GameObject GameOver;
    bool isEndingDone = false;

	// Use this for initialization
	void Start () {
        LevelEventHandler.StartListening(EndingEventName, EndGame);
	}

    public void EnableReturn()
    {
        isEndingDone = true;
    }

    void EndGame()
    {
        GameOver.SetActive(true);
    }

     void Update()
    {
        if(isEndingDone && (Input.GetButton("Jump") || Input.GetButton("Start"))){
            GameInstance.LoadGameMode(TitleScreen);
        }

    }

}
