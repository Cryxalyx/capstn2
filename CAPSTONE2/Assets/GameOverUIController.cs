﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverUIController : MonoBehaviour {

    public GameObject GameOverScreen;

	// Use this for initialization
	void Start () {
        LevelEventHandler.StartListening("PlayerDead", GameOver);
	}

    void GameOver()
    {
        GameOverScreen.SetActive(true);
    }

    private void OnDestroy()
    {
        LevelEventHandler.StopListening("PlayerDead", GameOver);
    }
}
