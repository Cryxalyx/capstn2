using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Effects
{
    public class ExplosionPhysicsForce : MonoBehaviour
    {
        public float explosionForce;


        private IEnumerator Start()
        {
            // wait one frame because some explosions instantiate debris which should then
            // be pushed by physics force
            yield return null;

            float multiplier = GetComponent<ParticleSystemMultiplier>().multiplier;

            float radius = multiplier;
            float r = 10*multiplier;
            Collider[] cols = Physics.OverlapSphere(transform.position, radius);
            List<Rigidbody> rigidbodies = new List<Rigidbody>();
            foreach (Collider col in cols)
            {
                if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody))
                {
                    rigidbodies.Add(col.attachedRigidbody);
                }
            }
            
            foreach (Rigidbody rb in rigidbodies)
            {
                Vector3 force = rb.transform.position - transform.position;
                force.y = 2;
                rb.AddForce(force.normalized * explosionForce * rb.mass ,ForceMode.Impulse);
            }
        }
    }
}
