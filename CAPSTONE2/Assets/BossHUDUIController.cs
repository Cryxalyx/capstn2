﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHUDUIController : MonoBehaviour {

    public Image Icon;
    private BossLevelScene bossLevelScene;

    private void Start()
    {
        bossLevelScene = GameInstance.GetActiveGameMode<BossLevelScene>();
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        Icon.sprite = bossLevelScene.Boss.GetComponent<BossBehavior>().UIBossData.Icon;
        bossLevelScene.Boss.GetComponent<BossBehavior>().SetUpUI();
    }
}
