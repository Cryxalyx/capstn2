﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffectSpawner : Spawner {

   
    public GameObject ExplosionEffect;
    float m_spawnTimer;
    float m_spawnTime;
	// Use this for initialization
	void Start () {

    }
	
    bool ShouldSpawn()
    {
        m_spawnTimer += Time.deltaTime;
        return (m_spawnTimer > m_spawnTime);
    }

    public override Vector3 RandomSpawnPoint()
    {
        Vector3 newPoint = base.RandomSpawnPoint();
        newPoint.y = newPoint.y + Random.Range(-m_radius, m_radius);
        return newPoint;
    }

    // Update is called once per frame
    void Update () {
		if(ShouldSpawn())
        {
            Spawn(ExplosionEffect);
            m_spawnTime = Random.Range(m_MinSpawnTime, m_MaxSpawnTime);
            m_spawnTimer = 0;
        }
	}

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, m_radius);
    }
}
